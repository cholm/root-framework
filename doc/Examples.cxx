//
// $Id: Examples.cxx,v 1.6 2005-12-02 14:41:43 cholm Exp $
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   Examples.cxx
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Examples. */


/** @page examples Examples 
    Here are the various examples.
 */

/** @example Header.h
    @par Declaration of Header data class. */
/** @example Header.cxx
    @par Implementation of Header data class. */
/** @example Point.h
    @par Declaration of Point data class. */
/** @example Point.cxx
    @par Implementation of Point data class. */
/** @example Line.h
    @par Declaration of Line data class. */
/** @example Line.cxx
    @par Implementation of Line data class. */
/** @example HeaderWriter.h
    @par Declaration of HeaderWriter task class. */
/** @example HeaderWriter.cxx
    @par Implementation of HeaderWriter task class. */
/** @example HeaderReader.h
    @par Declaration of HeaderReader task class. */
/** @example HeaderReader.cxx
    @par Implementation of HeaderReader task class. */
/** @example PointWriter.h
    @par Declaration of PointWriter task class. */
/** @example PointWriter.cxx
    @par Implementation of PointWriter task class. */
/** @example PointReader.h
    @par Declaration of PointReader task class. */
/** @example PointReader.cxx
    @par Implementation of PointReader task class. */
/** @example LineWriter.h
    @par Declaration of LineWriter task class. */
/** @example LineWriter.cxx
    @par Implementation of LineWriter task class. */
/** @example ChainWriterConfig.C
    @par Configuration script create a header, and points in a number
    of files (chain). */
/** @example ChainReaderConfig.C
    @par Configuration script to read a header and points from a set
    of files (chain). */
/** @example WriterConfig.C
    @par Configuration script create points and lines. */
/** @example ReaderConfig.C
    @par Configuration script to read points. */
/** @example ReadWriteConfig.C
    @par Configuration script to read points and write lines. */
/** @example ShowTree.C
    @par Script to show contents of tree. */
/** @example Break.C
    @par Script to show break feature. */
/** @example DelayReaderConfig.C
    @par Configuration script to read a header and points from a tree
    and read the previous entry of the header and points. */

//
// EOF
//
