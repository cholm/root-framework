//
// : docroot,v 1.1 2004/12/16 01:24:23 cholm Exp $
//
//   Example offline project
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License
//   as published by the Free Software Foundation; either version 2 of
//   the License, or (at your option) any later version.
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//   MA 02111-1307  USA
//
//
/** @file   ROOT.cxx
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  ROOT class documentation. */
/** @defgroup ROOT ROOT classes referenced */
/** @class   TObject ROOT.cxx <TObject.h>
    @ingroup ROOT
    @brief   ROOT class TObject
    See also <a href='http://root.cern.ch/root/htmldoc/TObject.html'>Documentation of TObject</a>
 */
class TObject {};
/** @class   TNamed ROOT.cxx <TNamed.h>
    @ingroup ROOT
    @brief   ROOT class TNamed
    See also <a href='http://root.cern.ch/root/htmldoc/TNamed.html'>Documentation of TNamed</a>
 */
class TNamed : public TObject {};

/** @class   TTask ROOT.cxx <TTask.h>
    @ingroup ROOT
    @brief   ROOT class TTask
    See also <a href='http://root.cern.ch/root/htmldoc/TTask.html'>Documentation of TTask</a>
 */
class TTask : public TNamed {};

/** @class   TClonesArray ROOT.cxx <TClonesArray.h>
    @ingroup ROOT
    @brief   ROOT class TClonesArray
    See also <a href='http://root.cern.ch/root/htmldoc/TClonesArray.html'>Documentation of TClonesArray</a>
 */
class TClonesArray : public TNamed {};

/** @class   TTree ROOT.cxx <TTree.h>
    @ingroup ROOT
    @brief   ROOT class TTree
    See also <a href='http://root.cern.ch/root/htmldoc/TTree.html'>Documentation of TTree</a>
 */
class TTree : public TNamed {};

/** @class   TChain ROOT.cxx <TChain.h>
    @ingroup ROOT
    @brief   ROOT class TChain
    See also <a href='http://root.cern.ch/root/htmldoc/TChain.html'>Documentation of TChain</a>
 */
class TChain : public TTree {};

/** @class   TFile ROOT.cxx <TFile.h>
    @ingroup ROOT
    @brief   ROOT class TFile
    See also <a href='http://root.cern.ch/root/htmldoc/TFile.html'>Documentation of TFile</a>
 */
class TChain : public TTree {};

/** @class   TFolder ROOT.cxx <TFolder.h>
    @ingroup ROOT
    @brief   ROOT class TFolder
    See also <a href='http://root.cern.ch/root/htmldoc/TFolder.html'>Documentation of TFolder</a>
 */
class TFolder : public TNamed {};

// EOF
