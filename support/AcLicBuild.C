void AcLicBuild()
{
  // Customize 
  const char* srcs[] = {
    "DebugGuard",
    "Task",
    "Main",
    "ArrayIO",
    "ObjectIO",
    "TreeIO",
    "ChainIO" };
  const char* libnam = "libFramework";
  const char* dirnam = "framework";
  const char* tmpnam = "Framework.C";

  gSystem->Load("libFramework.so");
  
  // This part is generic 
  std::ofstream tmp(tmpnam);
  for (auto src : srcs)
    tmp << "#include \"" << dirnam << "/" << src << ".cxx\"" << std::endl;

  tmp << "#include \"" << dirnam << "/Linkdef.h\"" << std::endl;
  
  tmp.close();
  
  gSystem->AddIncludePath("-I.");
  gSystem->CompileMacro(tmpnam,"kg",libnam);
  // gSystem->Unlink(tmpnam);
}

  
    
    
