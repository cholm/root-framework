dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: framework.m4,v 1.6 2006-02-22 15:32:14 cholm Exp $ 
dnl  
dnl   ROOT generic framework
dnl   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_ROOT_FRAMEWORK([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_FRAMEWORK],
[
    AC_ARG_WITH([framework-prefix],
        [AC_HELP_STRING([--with-framework-prefix],
		[Prefix where Framework is installed])],
        framework_prefix=$withval, framework_prefix="")

    if test "x${FRAMEWORK_CONFIG+set}" != xset ; then 
        if test "x$framework_prefix" != "x" ; then 
	    FRAMEWORK_CONFIG=$framework_prefix/bin/framework-config
	fi
    fi   
    AC_ARG_WITH([framework-url],
        [AC_HELP_STRING([--with-framework-url],
		[Base URL where the Framework dodumentation is installed])],
        framework_url=$withval, framework_url="")

    if test "x${FRAMEWORK_CONFIG+set}" != xset ; then 
        if test "x$framework_prefix" != "x" ; then 
	    FRAMEWORK_CONFIG=$framework_prefix/bin/framework-config
	fi
    fi   
    AC_PATH_PROG(FRAMEWORK_CONFIG, framework-config, no)
    framework_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Framework version >= $framework_min_version)

    framework_found=no    
    if test "x$FRAMEWORK_CONFIG" != "xno" ; then 
       FRAMEWORK_CPPFLAGS=`$FRAMEWORK_CONFIG --cppflags`
       FRAMEWORK_INCLUDEDIR=`$FRAMEWORK_CONFIG --includedir`
       FRAMEWORK_LIBS=`$FRAMEWORK_CONFIG --libs`
       FRAMEWORK_LIBDIR=`$FRAMEWORK_CONFIG --libdir`
       FRAMEWORK_LDFLAGS=`$FRAMEWORK_CONFIG --ldflags`
       FRAMEWORK_PREFIX=`$FRAMEWORK_CONFIG --prefix`
       
       framework_version=`$FRAMEWORK_CONFIG -V` 
       framework_vers=`echo $framework_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       framework_regu=`echo $framework_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $framework_vers -ge $framework_regu ; then 
            framework_found=yes
       fi
    fi
    AC_MSG_RESULT($framework_found - is $framework_version) 
    AC_MSG_CHECKING(where the Framework documentation is installed)
    if test "x$framework_url" = "x" && \
	test ! "x$FRAMEWORK_PREFIX" = "x" ; then 
       FRAMEWORK_URL=${FRAMEWORK_PREFIX}/share/doc/framework/html
    else 
	FRAMEWORK_URL=$framework_url
    fi	
    AC_MSG_RESULT($FRAMEWORK_URL)
  
    if test "x$framework_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(FRAMEWORK_URL)
    AC_SUBST(FRAMEWORK_PREFIX)
    AC_SUBST(FRAMEWORK_CPPFLAGS)
    AC_SUBST(FRAMEWORK_INCLUDEDIR)
    AC_SUBST(FRAMEWORK_LDFLAGS)
    AC_SUBST(FRAMEWORK_LIBDIR)
    AC_SUBST(FRAMEWORK_LIBS)
])

dnl
dnl EOF
dnl 
