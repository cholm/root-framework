// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: Linkdef.h,v 1.5 2004-12-29 23:53:52 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#if !defined(__CINT__) and !defined(CLING)
// #error Not for compilation
#else

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace Example;
#pragma link C++ class     Example::Header+;
#pragma link C++ class     Example::Point+;
#pragma link C++ class     Example::Line+;
#pragma link C++ class     Example::HeaderWriter;
#pragma link C++ class     Example::HeaderReader;
#pragma link C++ class     Example::PointWriter;
#pragma link C++ class     Example::PointReader;
#pragma link C++ class     Example::LineWriter;
#pragma link C++ class     Example::LineReader;
#endif

//____________________________________________________________________ 
//  
// EOF
//
