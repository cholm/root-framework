// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_Header
#define Example_Header
#include <TObject.h>

namespace Example 
{
  /** @class Header example/Header.h <example/Header.h>
      @brief An event header
   */
  class Header : public TObject
  {
  protected:
    /** Event number */
    Int_t fNumber;
  public:
    /** Default CTOR */
    Header(Int_t no=0) : fNumber(no) {}
    /** DTOR - does nothing  */
    virtual ~Header() {}
    
    /** @return Get the event number  */
    Int_t Number() const { return fNumber;  }
    /** @param no The event number  */
    void SetNumber(Int_t no=0) { fNumber = no; }
    
    /** Print information on point 
	@param option Not used  */
    virtual void     Print(Option_t* option="E") const; //*MENU*

    ClassDef(Header,1) // An event header 
  };
}
#endif
//____________________________________________________________________ 
//  
// EOF
//
