// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_HeaderReader
#define Example_HeaderReader
#include <framework/ObjectIO.h>

namespace Example 
{
  /** @class HeaderReader example/HeaderReader.h <example/HeaderReader.h>
      @brief Task to read Header objects
   */
  class HeaderReader : public Framework::ObjectReader
  {
  public:
    /** Construct a point reader
	@param name Name of the task and folder to post to
	@param title Title of the task */
    HeaderReader(const Char_t* name="", const Char_t* title="");
    virtual ~HeaderReader() {}
    
    /** Read points from the branch and post them to the folder
	specified by the task name 
	@param option Not used */
    virtual void Exec(Option_t* option=""); 
    
    ClassDef(HeaderReader,1) // Read Headers into TFolder 
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
