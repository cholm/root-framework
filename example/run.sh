#!/bin/sh

while test $# -gt 0 ; do
    case $1 in
	-L) libpath="$2" ; shift ;;
	-I) incpath="$2" ; shift ;;
	-c) root="$2" ; shift ;;
	-r) run="$2" ; shift ;; 
	*) break ;;
    esac
    shift
done

cat <<EOF
LD_LIBRARY_PATH:	$libpath:$LD_LIBRARY_PATH
ROOT_INCLUDE_PATH:	$incpath:$ROOT_INCLUDE_PATH
root:			$root 

Arguments:

	$@
EOF

LD_LIBRARY_PATH="$libpath:$LD_LIBRARY_PATH" \
	       ROOT_INCLUDE_PATH="$incpath:$ROOT_INCLUDE_PATH" \
	       $root -b -q -l $@ $run

