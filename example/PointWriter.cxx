//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#include <TClonesArray.h>
#include <TRandom.h>
#include <TBranch.h>
#include <TTree.h>
#include "PointWriter.h"
#include "Point.h"

//____________________________________________________________________
// ClassImp(Example::PointWriter);

//____________________________________________________________________
Example::PointWriter::PointWriter(const Char_t* name, 
				  const Char_t* title)
  : Framework::ArrayWriter(name, "Example::Point", name, title)
{
  // Default constructor
  fRandom = new TRandom;
  SetMeanX();
  SetMeanY();
  SetMeanZ();
  SetSigmaX();
  SetSigmaY();
  SetSigmaZ();
  SetMaxEntry(10);
}

//____________________________________________________________________
void 
Example::PointWriter::Exec(Option_t* option) 
{
  static Int_t entry = 0;
  if (++entry > fMaxEntry) { 
    Stop("Exec", "max events reached");
    return;
  }

  Int_t n = fRandom->Poisson(10); 

  TClonesArray& cache = *static_cast<TClonesArray*>(fCache);
  fCache->Clear();

  for (Int_t i = 0; i < n; i++) {
    Double_t x = fRandom->Gaus(fMeanX,fSigmaX);
    Double_t y = fRandom->Gaus(fMeanY,fSigmaY);
    Double_t z = fRandom->Gaus(fMeanZ,fSigmaZ);
    Point* point = new(cache[i]) Point(x, y, z, 
				       TMath::Abs(fMeanX - x), 
				       TMath::Abs(fMeanY - y), 
				       TMath::Abs(fMeanZ - z));
    Verbose(5, "Exec", "made point (%f,%f,%f)", 
	    point->X(), point->Y(), point->Z());
	    
  }
  Verbose(1, "Exec", "made %d space points", n);
  if (fDebug > 10)
    cache.ls();
}


//____________________________________________________________________ 
//  
// EOF
//
