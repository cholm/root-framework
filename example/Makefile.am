#
# $Id: Makefile.am,v 1.15 2006-05-14 16:14:30 cholm Exp $
#
#   ROOT generic framework
#   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation; either version 2.1
#  of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
#  02111-1307 USA
#
#

LIBNAME				= Example
PCM				= Example_rdict.pcm
MAP				= Example.map
bogusdir			= $(addprefix $(PWD)/, \
				    @top_builddir@/example/.bogus)

bogus_LTLIBRARIES		= Example.la
noinst_SCRIPTS			= run.sh
noinst_HEADERS			= Point.h		\
				  Line.h		\
				  Header.h		\
				  HeaderWriter.h	\
				  HeaderReader.h	\
				  PointWriter.h		\
				  PointReader.h		\
				  LineWriter.h		\
				  LineReader.h		\
				  Linkdef.h

Example_la_SOURCES		= Point.cxx		\
				  Line.cxx		\
				  Header.cxx		\
				  HeaderWriter.cxx	\
				  HeaderReader.cxx	\
				  PointWriter.cxx	\
				  PointReader.cxx	\
				  LineWriter.cxx	\
				  LineReader.cxx	
nodist_Example_la_SOURCES	= Example_dict.cxx
Example_la_LDFLAGS		= -L@ROOTLIBDIR@ 	\
				  -R @ROOTLIBDIR@ 	\
				  -avoid-version 	\
				  -module
Example_la_LIBADD		= -lPhysics -lMatrix \
				  $(top_builddir)/framework/libFramework.la

CLEANFILES			= $(LIBNAME)_dict.cxx $(LIBNAME)_dict.h \
				  *~ *_tmp_* $(PCM) *.root 
				  $(wildcard $(bogusdir)/*)
AM_CPPFLAGS			= -I@ROOTINCDIR@ -I$(top_srcdir)
AM_CXXFLAGS			= $(ROOTAUXCFLAGS)
EXTRA_DIST			= Break.C		\
				  ShowTree.C		\
				  ReaderConfig.C	\
				  WriterConfig.C	\
				  ReadWriteConfig.C	\
				  ChainWriterConfig.C	\
				  ChainReaderConfig.C	\
				  DelayReaderConfig.C	\
				  Run.C			


TESTS				= WriterConfig.C	\
			  	  ReaderConfig.C 	\
				  ReadWriteConfig.C 	\
				  ChainWriterConfig.C	\
				  ChainReaderConfig.C	\
				  DelayReaderConfig.C

dict_verbose			= $(dict_verbose_@AM_V@)
dict_verbose_			= $(dict_verbose_@AM_DEFAULT_V@)
dict_verbose_0			= @echo "  DICT     $@";

LOG_COMPILER = $(srcdir)/run.sh
AM_LOG_FLAGS = -L $(top_builddir)/$(PACKAGE)/.libs:$(builddir)/.libs	\
	       -I $(top_srcdir):$(srcdir) \
	       -c $(ROOTEXEC) \
	       -r $(srcdir)/Run.C

$(LIBNAME)_dict.h $(PCM):$(LIBNAME)_dict.cxx
	$(AM_V_at)if test -f $@; then :; else rm -f $^ ; \
	  $(MAKE) $(AM_MAKEFLAGS) $^; fi

$(LIBNAME)_dict.cxx:$(pkginclude_HEADERS) $(noinst_HEADERS)
if ROOT6
	$(AM_V_at)$(if $(findstring $(srcdir),.),, cp $^ .)
	$(dict_verbose)@ROOTCLING@ -f \
	  -s $(LIBNAME)$(MODEXT) \
	  -rml $(LIBNAME)$(MODEXT) \
	  -rmf $(LIBNAME).rootmap \
	  -DCLING $(AM_CPPFLAGS) $(INCLUDES) $(DEFS) \
	  $@ $(notdir $^) && \
	  (cd .libs && rm -f $(PCM) && ln -s ../$(PCM) .)
	  (cd .libs && rm -f $(MAP) && ln -s ../$(MAP) .)
	$(AM_V_at)$(if $(findstring $(srcdir),.),, rm $(notdir $^))
else 
	@ROOTCINT@ -f $@ -c $(AM_CPPFLAGS) $(INCLUDES) $(DEFS) $^
endif


#
# EOF
#
