// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_Point
#define Example_Point
#include <TVector3.h>

namespace Example 
{
  /** @class Point example/Point.h <example/Point.h>
      @brief A point in 3D space 
   */
  class Point : public TVector3
  {
  protected:
    /** Error in X coordinate  */
    Double_t fEx;
    /** Error in Y coordinate  */
    Double_t fEy;
    /** Error in Z coordinate  */
    Double_t fEz;
  public:
    /** Default CTOR */
    Point() : TVector3(0,0,0) { fEx = fEy = fEz = 0; }
    /** Create a point 
	@param x x coordinate
	@param y y coordinate
	@param z z coordinate
	@param ex Error on x coordinate
	@param ey Error on y coordinate
	@param ez Error on z coordinate  */
    Point(Double_t x,    Double_t y,    Double_t z,
	  Double_t ex=0, Double_t ey=0, Double_t ez=0);
    /** Create a point from pair of 3D vectors 
	@param pos Position
	@param err Error  */
    Point(const TVector3& pos, const TVector3& err);
    /** Create a point from pair of 3D arrays
	@param pos Coordinates 
	@param err Errors   */
    Point(Double_t* pos, Double_t* err);
    /** DTOR - does nothing  */
    virtual ~Point() {}
    
    /** @return  Error on x coordinate */
    virtual Double_t Ex() const { return fEx; }
    /** @return  Error on y coordinate */
    virtual Double_t Ey() const { return fEy; }
    /** @return  Error on z coordinate */
    virtual Double_t Ez() const { return fEz; }
    /** @param x  Error on x coordinate */
    virtual void     SetEx(Double_t x) { fEx = x; }
    /** @param y  Error on y coordinate */
    virtual void     SetEy(Double_t y) { fEy = y; }
    /** @param z  Error on z coordinate */
    virtual void     SetEz(Double_t z) { fEz = z; }
    
    /** Print information on point 
	@param option Not used  */
    virtual void     Print(Option_t* option="E") const; //*MENU*

    ClassDef(Point,1) // A 3D point with errors
  };
}
#endif
//____________________________________________________________________ 
//  
// EOF
//
