// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id: HeaderWriter.h,v 1.1 2004-12-29 23:53:52 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_HeaderWriter
#define Example_HeaderWriter
#include <framework/ObjectIO.h>
#include <TRandom.h>

namespace Example 
{
  /** @class HeaderWriter example/HeaderWriter.h <example/HeaderWriter.h>
      @brief Task to write Header objects
   */
  class HeaderWriter : public Framework::ObjectWriter
  {
  private:
  public:
    /** Create a point writer
	@param name Name of task, and folder to post to 
	@param title Title of the task.  */
    HeaderWriter(const Char_t* name="", const Char_t* title="");
    /** Destrcutor - does nothing  */
    virtual ~HeaderWriter() {}
    
    /** Create a random number of space points, and store them in the
	output tree and post them in folder 
	@param option Not used  */
    virtual void Exec(Option_t* option=""); 
    ClassDef(HeaderWriter,1) // Write Header to TTree and TFolder
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
