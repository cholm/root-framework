// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_LineWriter
#define Example_LineWriter
#include <framework/ArrayIO.h>

namespace Example 
{
  /** @class LineWriter example/LineWriter.h <example/LineWriter.h>
      @brief Task to write Line objects
   */
  class LineWriter : public Framework::ArrayWriter
  {
  public:
    /** Construct a line writer 
	@param name Name of the task 
	@param title Title of the task */
    LineWriter(const Char_t* name="", const Char_t* title="");
    /** Destructor - does nothing  */
    virtual ~LineWriter() {}
    
    /** Pair up points from the folder @c "points" in a line, and
	store the lines in the tree and post them on the folder @c
	"lines" 
	@param option Not used 
    */
    virtual void Exec(Option_t* option=""); 
    
    ClassDef(LineWriter,1) // Write 3D line to TTree
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
