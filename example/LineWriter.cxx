//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#include <TClonesArray.h>
#include <TBranch.h>
#include <TTree.h>
#include <TFolder.h>
#include "LineWriter.h"
#include "Line.h"
#include "Point.h"

//____________________________________________________________________
// ClassImp(Example::LineWriter);

//____________________________________________________________________
Example::LineWriter::LineWriter(const Char_t* name, const Char_t* title)
  : Framework::ArrayWriter(name, "Example::Line", name, title)
{}

//____________________________________________________________________
void 
Example::LineWriter::Exec(Option_t* option) 
{
  TFolder* folder = GetBaseFolder();
  if (!folder) 
    return;
  folder = static_cast<TFolder*>(folder->FindObject("points"));
  if (!folder) {
    Debug(1, "Exec", "couldn't find the points folder");
    return;
  }
  folder->ls();
  folder->GetListOfFolders()->ls();
  TClonesArray* points = 
    static_cast<TClonesArray*>(folder->GetListOfFolders());
  
  Int_t n = points->GetEntries();
  Verbose(1, "Exec", "got %d space points", n);
  
  fCache->Clear();
  TClonesArray& cache = *static_cast<TClonesArray*>(fCache);

  for (Int_t i = 0; i < n / 2; i++) {
    Point* p1 = static_cast<Point*>(points->At(2 * i));
    Point* p2 = static_cast<Point*>(points->At(2 * i + 1));
    if (!p1 || !p2) {
      Debug(1, "Exec", "couldn't get the # %d pair of points", i);
      break;
    }
    Line* l = new (cache[i]) Line(*p1, *p2, kTRUE);
    l->AddPoint(p1);
    l->AddPoint(p2);
  }
  Verbose(1, "Exec", "made %d lines", n/2);
}

//____________________________________________________________________ 
//  
// EOF
//
