//____________________________________________________________________ 
//  
// $Id: Line.cxx,v 1.5 2007-08-05 09:25:48 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#include "Line.h"
#include <TROOT.h>
#include <TMath.h>
#include <iostream>
#include <iomanip>

//____________________________________________________________________
// ClassImp(Example::Line);

//____________________________________________________________________
Example::Line::Line(Double_t x,  Double_t y,  Double_t z, 
		    Double_t a,  Double_t b,  Double_t c, 
		    Double_t ex, Double_t ey, Double_t ez, 
		    Double_t ea, Double_t eb, Double_t ec)
  : Point(x,y,z,ex,ey,ez), fDx(a), fDy(b), fDz(c), 
    fEdx(ea), fEdy(eb), fEdz(ec)
{
  // Default constructor
  fPoints = new TRefArray;
}

//____________________________________________________________________
Example::Line::Line(const Point& p1, const Point& p2,  Bool_t points) 
  : Point(p1)
{
  if (!points) {
    fDx  = p2.X();
    fDy  = p2.Y();
    fDz  = p2.Z();
    fEdx = p2.Ex();
    fEdy = p2.Ey();
    fEdz = p2.Ez();
    return;
  }
  fDx = X() - p2.X();
  fDy = Y() - p2.Y();
  fDz = Z() - p2.Z();
  
  Double_t l = TMath::Sqrt(fDx*fDx + fDy*fDy + fDz*fDz);
  fDx /= l;
  fDy /= l; 
  fDz /= l;
  fEdx = 0;
  fEdy = 0;
  fEdz = 0;
  
  fPoints = new TRefArray;
}

//____________________________________________________________________
void 
Example::Line::Print(Option_t* option) const 
{
  Bool_t optionE = kFALSE;
  Bool_t optionR = kFALSE;
  Bool_t optionA = kFALSE;
  TString opt(option);
  if (opt.Contains("e", TString::kIgnoreCase)) optionE = kTRUE;
  if (opt.Contains("r", TString::kIgnoreCase)) optionR = kTRUE;
  if (opt.Contains("a", TString::kIgnoreCase)) optionA = kTRUE;

  TROOT::IndentLevel();
  std::cout << "Line:    (" << X();
  if (optionE) std::cout << " +/- " << Ex();
  std::cout << "," << Y();
  if (optionE) std::cout << " +/- " << Ey();
  std::cout << "," << Z();
  if (optionE) std::cout << " +/- " << Ez();
  std::cout << ") + t * (" << fDx;
  if (optionE) std::cout << " +/- " << fEdx;
  std::cout << "," << fDy;
  if (optionE) std::cout << " +/- " << fEdy;
  std::cout << "," << fDz;
  if (optionE) std::cout << " +/- " << fEdz;
  std::cout << ")";
  if (optionA) std::cout << " [@" << std::hex << this << "]";
  std::cout << std::endl; 

  if (!optionR) return;
  TROOT::IndentLevel();
  std::cout << "  Points: (" << fPoints->GetEntries() << ")" << std::endl;
  TROOT::IncreaseDirLevel();
  TIter next(fPoints);
  Point* p = 0;
  while ((p = (Point*)next())) 
    p->Print(option);
  
  TROOT::DecreaseDirLevel();
}


//____________________________________________________________________ 
//  
// EOF
//
