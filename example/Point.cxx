//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#include "Point.h"
#include <iostream>
#include <iomanip>
#include <TROOT.h>

//____________________________________________________________________
// ClassImp(Example::Point);

//____________________________________________________________________
Example::Point::Point(Double_t x,  Double_t y,  Double_t z,
		      Double_t ex, Double_t ey, Double_t ez)
  : TVector3(x, y, z), 
    fEx(ex), 
    fEy(ey), 
    fEz(ez)
{
  // Default constructor
}

//____________________________________________________________________
Example::Point::Point(Double_t* pos, Double_t* err)
  : TVector3(pos[0], pos[1], pos[2]), 
    fEx(err[0]), 
    fEy(err[1]), 
    fEz(err[2]) 
{
  // Default constructor
}

//____________________________________________________________________
Example::Point::Point(const TVector3& pos, const TVector3& err)
  : TVector3(pos)
{
  // Default constructor
  fEx = err.X();
  fEy = err.Y();
  fEz = err.Z();
}

//____________________________________________________________________
void 
Example::Point::Print(Option_t* option) const 
{
  Bool_t optionE = kFALSE;
  Bool_t optionA = kFALSE;
  TString opt(option);
  if (opt.Contains("e", TString::kIgnoreCase)) optionE = kTRUE;
  if (opt.Contains("a", TString::kIgnoreCase)) optionA = kTRUE;

  TROOT::IndentLevel();
  std::cout << "  (" << X();
  if (optionE) std::cout << " +/- " << fEx;
  std::cout << "," << Y();
  if (optionE) std::cout << " +/- " << fEy;
  std::cout << "," << Z();
  if (optionE) std::cout << " +/- " << fEz;
  std::cout << ")";
  if (optionA) std::cout << " [@" << std::hex << this << "]";
  std::cout << std::endl; 
}

  
//____________________________________________________________________ 
//  
// EOF
//
