// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef EXAMPLE_Line
#define EXAMPLE_Line
#include "Point.h"
#include <TRefArray.h>

namespace Example 
{
  /** @class Line example/Line.h <example/Line.h>
      @brief a 3D line between two Point's  */
  class Line : public Point
  {
  protected:
    /** Slope in x */
    Double_t   fDx;
    /** Slope in y */
    Double_t   fDy;
    /** Slope in z */
    Double_t   fDz;
    /** Error on slope in x */
    Double_t   fEdx;
    /** Error on slope in y */
    Double_t   fEdy;
    /** Error on slope in z */
    Double_t   fEdz;
    
    /** References to points that make up this line  */
    TRefArray* fPoints;
  public:
    /** Create a Line   */
    Line() : Point(0,0,0,0,0,0) { fDx = fDy = fDz = fEdx = fEdy =
				    fEdz = 0; fPoints = 0; }
    /** Create a line 
	@param x Start x coordinate
	@param y Start y coordinate
	@param z Start z coordinate
	@param a Slope in x
	@param b Slope in y
	@param c Slope in z
	@param ex Error on start x coordinate
	@param ey Error on start y coordinate
	@param ez Error on start z coordinate
	@param ea Error on slope in x
	@param eb Error on slope in y
	@param ec Error on slope in z */
    Line(Double_t x,    Double_t y,    Double_t z, 
	 Double_t a,    Double_t b,    Double_t c, 
	 Double_t ex=0, Double_t ey=0, Double_t ez=0, 
	 Double_t ea=0, Double_t eb=0, Double_t ec=0);
    /** Create a Line from two points 
	@param p1 Point 1
	@param p2 Point 2
	@param points Whether to store the points   */
    Line(const Point& p1, const Point& p2,  Bool_t points=kTRUE); 
    /** Destructor   */
    virtual ~Line() {}
    
    /** @return  Slope in x */
    virtual Double_t       Dx() const { return fDx; }
    /** @return  Slope in y */
    virtual Double_t       Dy() const { return fDy; }
    /** @return  Slope in z */
    virtual Double_t       Dz() const { return fDz; }
    /** @return  Error on slope in x */
    virtual Double_t       Edx() const { return fEdz; }
    /** @return  Error on slope in y */
    virtual Double_t       Edy() const { return fEdy; }
    /** @return  Error on slope in z */
    virtual Double_t       Edz() const { return fEdz; }
    
    /** @return List of points that make up the line  */
    virtual TRefArray*     GetPoints() const { return fPoints; }
    /** @param p Point to add to make up the line  */
    virtual void           AddPoint(Point* p) { fPoints->Add(p); }
    
    /** Print information on the line 
	@param option Not used  */
    virtual void           Print(Option_t* option="") const; //*MENU*
    
    ClassDef(Line,1) // a 3D line with errors
  };
}
#endif
//____________________________________________________________________ 
//  
// EOF
//
