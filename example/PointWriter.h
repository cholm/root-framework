// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_PointWriter
#define Example_PointWriter
#include <framework/ArrayIO.h>
#include <TRandom.h>

namespace Example 
{
  /** @class PointWriter example/PointWriter.h <example/PointWriter.h>
      @brief Task to write Point objects
   */
  class PointWriter : public Framework::ArrayWriter
  {
  private:
    /** Random number generator */
    TRandom* fRandom;
    /** @f$ \langle x\rangle@f$ */
    Double_t fMeanX;
    /** @f$ \langle y\rangle@f$ */
    Double_t fMeanY;
    /** @f$ \langle z\rangle@f$ */
    Double_t fMeanZ;
    /** @f$ \sigma_x@f$ */
    Double_t fSigmaX;
    /** @f$ \sigma_y@f$ */
    Double_t fSigmaY;
    /** @f$ \sigma_z@f$ */
    Double_t fSigmaZ;    
    /** Max number of points to make */
    Int_t    fMaxEntry;
  public:
    /** Create a point writer
	@param name Name of task, and folder to post to 
	@param title Title of the task.  */
    PointWriter(const Char_t* name="", const Char_t* title="");
    /** Destrcutor - does nothing  */
    virtual ~PointWriter() {}
    
    /** @param x @f$ \langle x\rangle@f$ */
    void     SetMeanX(Double_t x=5)  { fMeanX = x; }    //*MENU*
    /** @param x @f$ \langle y\rangle@f$ */
    void     SetMeanY(Double_t x=5)  { fMeanY = x; }    //*MENU*
    /** @param x @f$ \langle z\rangle@f$ */
    void     SetMeanZ(Double_t x=5)  { fMeanZ = x; }    //*MENU*
    /** @param x @f$ \sigma_x@f$ */
    void     SetSigmaX(Double_t x=1) { fSigmaX = x; }   //*MENU*
    /** @param x @f$ \sigma_y@f$ */
    void     SetSigmaY(Double_t x=1) { fSigmaY = x; }   //*MENU*
    /** @param x @f$ \sigma_z@f$ */
    void     SetSigmaZ(Double_t x=1) { fSigmaZ = x; }   //*MENU* 
    /** @param n Max number of points to make */
    void     SetMaxEntry(Int_t n=10) { fMaxEntry = n; } //*MENU*
    /** @return @f$ \langle x\rangle@f$ */
    Double_t GetMeanX() const { return fMeanX; } 
    /** @return @f$ \langle y\rangle@f$ */
    Double_t GetMeanY() const { return fMeanY; }
    /** @return @f$ \langle z\rangle@f$ */
    Double_t GetMeanZ() const { return fMeanZ; }
    /** @return @f$ \sigma_x@f$ */
    Double_t GetSigmaX() const { return fSigmaX; }
    /** @return @f$ \sigma_y@f$ */
    Double_t GetSigmaY() const { return fSigmaY; }
    /** @return @f$ \sigma_z@f$ */
    Double_t GetSigmaZ() const { return fSigmaZ; }    
    /** @return Max number of points to make */
    Int_t    GetMaxEntry() const { return fMaxEntry; }

    /** Create a random number of space points, and store them in the
	output tree and post them in folder 
	@param option Not used  */
    virtual void Exec(Option_t* option=""); 
    ClassDef(PointWriter,1) // Write Point to TTree and TFolder
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
