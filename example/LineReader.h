// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id: LineReader.h,v 1.6 2004-12-15 23:49:07 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_LineReader
#define Example_LineReader
#include <framework/ArrayIO.h>

namespace Example 
{
  /** @class LineReader example/LineReader.h <example/LineReader.h>
      @brief Task to read Line objects
   */
  class LineReader : public Framework::ArrayReader
  {
  public:
    /** Construct a point reader
	@param name Name of the task and folder to post to
	@param title Title of the task */
    LineReader(const Char_t* name="", const Char_t* title="");
    virtual ~LineReader() {}
    
    /** Read points from the branch and post them to the folder
	specified by the task name 
	@param option Not used */
    virtual void Exec(Option_t* option=""); 
    
    ClassDef(LineReader,1) // Read Lines into TFolder 
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
