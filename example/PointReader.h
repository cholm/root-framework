// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_PointReader
#define Example_PointReader
#include <framework/ArrayIO.h>

namespace Example 
{
  /** @class PointReader example/PointReader.h <example/PointReader.h>
      @brief Task to read Point objects
   */
  class PointReader : public Framework::ArrayReader
  {
  public:
    /** Construct a point reader
	@param name Name of the task and folder to post to
	@param title Title of the task */
    PointReader(const Char_t* name="", const Char_t* title="");
    virtual ~PointReader() {}
    
    /** Read points from the branch and post them to the folder
	specified by the task name 
	@param option Not used */
    virtual void Exec(Option_t* option=""); 
    
    ClassDef(PointReader,1) // Read Points into TFolder 
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
