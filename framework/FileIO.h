// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/FileIO.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:15 2004
    @brief   FileIO declaration file 
*/
#ifndef Framework_FileIO
#define Framework_FileIO
#include <framework/Task.h>
#include <TString.h>
class TFile;

namespace Framework 
{
  //==================================================================
  /** @class FileIO framework/FileIO.h <framework/FileIO.h>
      @ingroup fw_special_tasks
      @brief Base class for TClonesArray reader and writers 
   */
  class FileIO : public Task 
  {
  protected:
    /** Name of the tree to make/get the branch in/from */
    TString fFileName;
    /** Name of the branch put/get the array in/from */
    TString fMode;
    /** Pointer to the file */
    TFile* fFile;
  public:
    /** Default constructor  */
    FileIO() : Task(), fFileName(""), fMode(""), fFile(0) {}
    /** Normal constructor 
	@param name Name of task and the folder it uses 
	@param fileName   Name of file
	@param mode       Open mode */
    FileIO(const Char_t* name, 
	   const Char_t* fileName, 
	   const Char_t* mode);
    /** Destructor  */
    virtual ~FileIO() { Close(); }
    /** Initialize the task.  The cache is initialized to a
	@c %TClonesArray of @c GetName classes. 
	@param option Not used.  Passed on to
	Framework::Task::Initialize  */
    virtual void Initialize(Option_t* option="");
    virtual void Register(Option_t* option="") {}
    virtual void Exec(Option_t* option="") {}
    virtual void Finish(Option_t* option="") { Close(); }
  protected:
    /** Close the file */
    virtual void Close();
    ClassDef(FileIO,1) // 
  };

  //==================================================================
  /** @class FileReader framework/FileIO.h <framework/FileIO.h>
      @ingroup fw_special_tasks
      @brief Task to read in an TClonesArray from a TTree 
   */
  class FileReader : public FileIO 
  {
  public:
    /** Default constructor  */
    FileReader() : FileIO() {}
    /** Normal constructor 
	@param name      Name of task and the folder it uses 
	@param fileName  Name of file to open */
    FileReader(const Char_t* name, const Char_t* fileName)
      : FileIO(name,fileName,"READ")
    {}
    /** Destructor  */
    virtual ~FileReader() {}

    ClassDef(FileReader,1) // 
  };

  //==================================================================
  /** @class FileWriter framework/FileIO.h <framework/FileIO.h>
      @ingroup fw_special_tasks
      @brief Task to read in an TClonesArray from a TTree 
   */
  class FileWriter : public FileIO 
  {
  public:
    /** Default constructor  */
    FileWriter() : FileIO() { }
    /** Normal constructor 
	@param name       Name of task and the folder it uses 
	@param filenName  Name of file to create */
    FileWriter(const Char_t* name,  const Char_t* fileName)
      : FileIO(name,fileName,"RECREATE")
    {}
    /** Destructor  */
    virtual ~FileWriter() {}
  protected:
    /** Close the file */
    virtual void Close();

    ClassDef(FileWriter,1) // 
  };
}

#endif
//
// EOF
//
  
      
