// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: Linkdef.h,v 1.7 2005-01-02 17:34:38 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Linkdef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:57 2004
    @brief   Linkage specifications 
*/
#if !defined(__CINT__) and !defined(CLING)
// #error Not for compilation
#else

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace Framework;
#pragma link C++ class     Framework::Main+;
#pragma link C++ class     Framework::Task+;
#pragma link C++ class     Framework::TreeIO+;
#pragma link C++ class     Framework::TreeReader+;
#pragma link C++ class     Framework::TreeWriter+;
#pragma link C++ class     Framework::DelayedTreeReader+;
#pragma link C++ class     Framework::ChainReader+;
#pragma link C++ class     Framework::ChainWriter+;
#pragma link C++ class     Framework::ArrayIO+;
#pragma link C++ class     Framework::ArrayReader+;
#pragma link C++ class     Framework::ArrayWriter+;
#pragma link C++ class     Framework::ObjectIO+;
#pragma link C++ class     Framework::ObjectReader+;
#pragma link C++ class     Framework::ObjectWriter+;
#pragma link C++ class     Framework::FileIO+;
#pragma link C++ class     Framework::FileReader+;
#pragma link C++ class     Framework::FileWriter+;


#endif
//____________________________________________________________________ 
//  
// EOF
//
