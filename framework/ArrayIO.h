// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/ArrayIO.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:15 2004
    @brief   ArrayIO declaration file 
*/
#ifndef Framework_ArrayIO
#define Framework_ArrayIO
#include <framework/Task.h>
#include <TString.h>


namespace Framework 
{
  //==================================================================
  /** @class ArrayIO framework/ArrayIO.h <framework/ArrayIO.h>
      @ingroup fw_special_tasks
      @brief Base class for TClonesArray reader and writers 
   */
  class ArrayIO : public Task 
  {
  protected:
    /** Name of the tree to make/get the branch in/from */
    TString fTreeName;
    /** Name of the branch put/get the array in/from */
    TString fBranchName;
  public:
    /** Default constructor  */
    ArrayIO();
    /** Normal constructor 
	@param name Name of task and the folder it uses 
	@param className  Class name of the objects to put in array
	@param branchName Name of the branch to put the array in
	@param treeName   Name of the tree that the branch should be in */
    ArrayIO(const Char_t* name, 
	    const Char_t* className, 
	    const Char_t* branchName=0, 
	    const Char_t* treeName=0);
    /** Destructor  */
    virtual ~ArrayIO() {}
    /** Set the tree's name 
	@param name Name of the tree */
    virtual void SetTreeName(const Char_t* name) { fTreeName = name; }
    /** Set the branch's name 
	@param name Name of the branch */
    virtual void SetBranchName(const Char_t* name) { fBranchName = name; }
    /** Initialize the task.  The cache is initialized to a
	@c %TClonesArray of @c GetName classes. 
	@param option Not used.  Passed on to
	Framework::Task::Initialize  */
    virtual void Initialize(Option_t* option="");
    virtual void Exec(Option_t* option="") {}
    
    ClassDef(ArrayIO,1) // 
  };

  //==================================================================
  /** @class ArrayReader framework/ArrayIO.h <framework/ArrayIO.h>
      @ingroup fw_special_tasks
      @brief Task to read in an TClonesArray from a TTree 
   */
  class ArrayReader : public ArrayIO 
  {
  public:
    /** Default constructor  */
    ArrayReader();
    /** Normal constructor 
	@param name Name of task and the folder it uses 
	@param className  Class name of the objects the in array
	@param branchName Name of the branch to read the array from
	@param treeName   Name of the tree that the branch is read from */
    ArrayReader(const Char_t* name,
		const Char_t* className, 
		const Char_t* branchName=0, 
		const Char_t* treeName=0);
    /** Destructor  */
    virtual ~ArrayReader() {}
    /** Regster the task.  The branch is obained from the tree, and
	the cache set to the read-in location. 
	@param option Not used.  Passed on to
	Framework::Task::Register  */
    virtual void Register(Option_t* option="");

    ClassDef(ArrayReader,1) // 
  };

  //==================================================================
  /** @class ArrayWriter framework/ArrayIO.h <framework/ArrayIO.h>
      @ingroup fw_special_tasks
      @brief Task to read in an TClonesArray from a TTree 
   */
  class ArrayWriter : public ArrayIO 
  {
  public:
    /** Default constructor  */
    ArrayWriter();
    /** Normal constructor 
	@param name Name of task and the folder it uses 
	@param className  Class name of the objects to put in array
	@param branchName Name of the branch to put the array in
	@param treeName   Name of the tree that the branch should be in */
    ArrayWriter(const Char_t* name, 
		const Char_t* className, 
		const Char_t* branchName=0, 
		const Char_t* treeName=0);
    /** Destructor  */
    virtual ~ArrayWriter() {}
    /** Regster the task.  The branch is obained from the tree, and
	the cache set to the read-in location. 
	@param option Not used.  Passed on to
	Framework::Task::Register  */
    virtual void Register(Option_t* option="");

    ClassDef(ArrayWriter,1) // 
  };
}

#endif
//
// EOF
//
  
      
