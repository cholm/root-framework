//____________________________________________________________________
//
//  ROOT generic framework
//  Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/ChainIO.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Dec 22 18:30:06 2004
    @brief   Implementation of ChainIO
*/
#include <framework/ChainIO.h>
#include <framework/Main.h>
#include "framework/DebugGuard.h"
#include <TBrowser.h>
#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TChainElement.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TFolder.h>
#include <TDirectory.h>
#include <iostream>
#include <fstream>
#include <iomanip>

#if 0
//====================================================================
ClassImp(Framework::ChainIO);

//____________________________________________________________________
Framework::ChainIO::ChainIO(const Char_t* chainName, 
			   const Char_t* title) 
  : Framework::Task(chainName, title),
    fBytes(0),
    fEntry(0)
{}

//____________________________________________________________________
Framework::ChainIO::~ChainIO()
{
  Finish();
}

//____________________________________________________________________
void
Framework::ChainIO::Initialize(Option_t* option)
{
  // Register a clones array as a branch and in a folder
  fFolder = Framework::Main::Instance()->GetFolder();
}

//____________________________________________________________________
void
Framework::ChainIO::Finish(Option_t* option)
{
  fBytes = 0;
  fEntry = 0;
}

//____________________________________________________________________
void
Framework::ChainIO::Print(Option_t* option) const 
{
  TString opt(option);
  Task::Print(option);
  if (opt.Contains("D", TString::kIgnoreCase)) {
    std::cout << "  Chain Name:   " << GetName() << "\n"
	      << "  Entry #:      " << fEntry    << "\n"
	      << "  Bytes:        " << fBytes    << std::endl;
  }
  if (opt.Contains("F", TString::kIgnoreCase) && 
      fTitle.Contains("XXXXXX", TString::kIgnoreCase))
    std::cout << "  File Pattern: " << GetTitle() << std::endl;
}
#endif 

//====================================================================
// ClassImp(Framework::ChainWriter);

//____________________________________________________________________
Framework::ChainWriter::ChainWriter(const Char_t* chainName, 
				    const Char_t* title) 
  : Framework::TreeWriter(chainName, title), 
    fFiles(0),
    fNextFile(0),
    fFileNumber(0),
    fMaxBytes(0),
    fMaxEntries(0)
{}

//____________________________________________________________________
Framework::ChainWriter::~ChainWriter()
{
  if (fFiles) {
    fFiles->Delete();
    delete fFiles;
  }
}

//____________________________________________________________________
void
Framework::ChainWriter::AddFile(const Char_t* fileName) 
{
  if (fNextFile) {
    Fail("AddFile", "file name iterator initialized; "
	 "no more additions possible");
    return;
  }
  if (!fFiles) fFiles = new TObjArray;
  fFiles->AddLast(new TObjString(fileName));
}


//____________________________________________________________________
void
Framework::ChainWriter::NewFile() 
{
  // Get the next file name 
  TObjString* fileName = 0;
  if (!fNextFile) {
    // If user didn't specify any files, then we make automatic file
    // names.  We subsitute the pattern `XXXXXX' with the file number
    // in the pattern given as the title of the task. 
    fileName = new TObjString(GetTitle());
    fileName->String().ReplaceAll("XXXXXX", Form("%06d", fFileNumber));
    if (!fFiles) fFiles = new TObjArray;
    fFiles->AddLast(fileName);
  }
  else 
    fileName = static_cast<TObjString*>(fNextFile->Next());
  fFileNumber++;
  if (!fileName) {
    Stop("NewFile", "No more files");
    return;
  }

  // Save the current directory. 
  TDirectory* savdir = gDirectory;

  // Open the new file 
  TFile* newFile = TFile::Open(fileName->String().Data(), "RECREATE");
  if (!newFile) {
    Fail("NewFile", "Failed to open the new file %s", 
	 fileName->String().Data());
    return;
  }

  // Make the tree.  
  TTree* oldTree = 0;
  if (!fTree) {
    // If no tree exists, this is the first time we get here, so we
    // make a new tree 
    DEBUGMSG(10, "New tree %s", GetName());
    fTree = new TTree(GetName(), GetName());
  }
  else {
    // Otherwise, we're making a clone of the existing tree. 
    if (fFolder) fFolder->Remove(fTree);
    DEBUGMSG(10, "cloning tree %s", fTree->GetName());
    // Save old tree 
    oldTree = fTree;
    // Clone the branches, And let the tree point to the new clone of
    // the old tree, and copy the addresses. 
    fTree = oldTree->CloneTree(0);
    // Remove clone from list of clones in old tree 
    TList* clones = oldTree->GetListOfClones();
    if (!clones->Remove(fTree)) 
      Warning("NewFile", "Couldn't remove clones %s from %s", 
	      fTree->GetName(), oldTree->GetName());
  }
  // Add the tree to the folder 
  if (fFolder) fFolder->Add(fTree);

  // Go the old file, and write out, and close
  if (fFile) {
    Verbose(20, "NewFile", "closing file %s @ entry %d (%d bytes)", 
	    fFile->GetName(), fEntry, fBytes);
    fFile->cd();
    fFile->Write();
    fFile->Close();
  }
  
  // Delete the old tree 
  // if (oldTree) delete oldTree;
  
  fFile = newFile;
  
  // Reset the bytes counter if we watching bytes
  if (fMaxBytes > 0) fBytes = 0;
  
  // Go back to old directory 
  savdir->cd();
}

//____________________________________________________________________
void
Framework::ChainWriter::Initialize(Option_t* option)
{
  // Register a clones array as a branch and in a folder
  fFolder = Framework::Main::Instance()->GetFolder();
  if (fFiles) fNextFile = new TIter(fFiles);
  NewFile();
}

//____________________________________________________________________
void
Framework::ChainWriter::Exec(Option_t* option)
{
  Bool_t needNewFile = kFALSE;
  if (fMaxEntries > 0 && 
      fEntry != 0 && fEntry % fMaxEntries == 0) needNewFile = kTRUE;
  if (fMaxBytes   > 0 && fBytes > fMaxBytes)    needNewFile = kTRUE;
  if (needNewFile) NewFile();

  TreeWriter::Exec(option);
}

//____________________________________________________________________
void
Framework::ChainWriter::Finish(Option_t* option)
{
  TreeWriter::Finish(option);
  if (fNextFile) {
    delete fNextFile;
    fNextFile = 0;
  }
  if (fFiles) {
    fFiles->Delete();
    delete fFiles;
    fFiles = 0;
  }
}

//____________________________________________________________________
void
Framework::ChainWriter::Print(Option_t* option) const 
{
  TreeWriter::Print(option);
  TString opt(option);
  if (opt.Contains("A", TString::kIgnoreCase) && fTree) {
    std::cout << std::setw(60) << "Branch" << " | " << std::setw(12) << "\n"
	      << std::setfill('-') << std::setw(60) << "-" 
	      << "-+-" << std::setw(12) << "-" << std::setfill(' ')
	      << std::endl;
    TObjArray *branches  = fTree->GetListOfBranches();
    Int_t nbranches = branches->GetEntries();
    for (Int_t i = 0; i < nbranches; i++) {
      TBranch *branch = (TBranch*)branches->At(i);
      if (branch->TestBit(kDoNotProcess)) continue;
      std::cout << std::setw(60) << branch->GetName() 
		<< " | " << std::setw(12) << std::hex
		<< ((void*)branch->GetAddress()) 
		<< std::dec << std::endl;
    }
  }
  if (opt.Contains("F", TString::kIgnoreCase) && fFiles) {
    std::cout << "Connected files: " << std::endl;
    TIter next(fFiles);
    TObjString* file = 0;
    while ((file = static_cast<TObjString*>(next()))) 
      std::cout << "  " << file->String() << std::endl;
  }
}

//____________________________________________________________________
void
Framework::ChainWriter::Browse(TBrowser* b) 
{
  Framework::TreeWriter::Browse(b);
  if (fFiles)  b->Add(fFiles, "Files");
}

//====================================================================
ClassImp(Framework::ChainReader);

//____________________________________________________________________
Framework::ChainReader::ChainReader(const Char_t* chainName, 
				   const Char_t* title) 
  : Framework::TreeReader(chainName, title)
{
  fTree = new TChain(chainName);
}


//____________________________________________________________________
void
Framework::ChainReader::AddFile(const Char_t* fileName) 
{
  Verbose(5,"AddFile","Adding file %s",fileName);
  TChain* chain = static_cast<TChain*>(fTree);
  chain->Add(fileName);
}

//____________________________________________________________________
void
Framework::ChainReader::AddFiles(const Char_t* listName) 
{
  Debug(5,"AddFiles","Reading files form \"%s\"", listName);
  std::ifstream in(listName);
  if (!in) {
    Warning("AddFiles", "Failed to open \"%s\"",listName);
    return;
  }
  Int_t cnt = 0;
  while (!in.eof()) {
    TString line;
    line.ReadLine(in);

    // Check empty line
    if (line.IsNull()) {
      Debug(5,"AddFiles", "Skipping empty line");
      continue;
    }

    // Check comment 
    TString sub = line.Strip(TString::kBoth);
    if (sub.BeginsWith("#")) {
      Debug(5,"AddFiles", "Skipping comment \"%s\"",line.Data());
      continue;
    }
    // Check for ending in .root(?)
    if (!sub.EndsWith(".root")) {
      Debug(5,"AddFiles","Skipping what doesn't look like a ROOT file \"%s\"",
	      sub.Data());
      continue;
    }

    // Add the file 
    AddFile(sub);
    cnt++;
  }
  Verbose(10,"AddFiles","Added %d files from \"%s\"",cnt,listName);
}

//____________________________________________________________________
void
Framework::ChainReader::Initialize(Option_t* option)
{
  // Register a clones array as a branch and in a folder
  fFolder = Framework::Main::Instance()->GetFolder();

  // Check that we have a tree 
  if (!fTree) {
    Fail("AddFile", "No chain");
    return;
  }

  // Check if we should add files automatically to the tree 
  TString title(GetTitle());
  if (title.Contains("XXXXXX", TString::kIgnoreCase)) {
    TChain* chain = static_cast<TChain*>(fTree);
    Int_t i = 0;
    do {
      TString tmp(GetTitle());
      tmp.ReplaceAll("XXXXXX", Form("%06d", i));
      char* test = gSystem->Which(".", tmp.Data(), kReadPermission);
      if (!test) break;
      chain->Add(tmp.Data());
      i++;
    }
    while (true);
    Verbose(5, "Initialize", "Added %d automatic files", i);
    if (fDebug > 50) Print("F");
  }

  // Add to folder 
  DEBUGMSG(50, "Adding TTree %s to folder %s",
	   fTree->GetName(), fFolder->GetName());
  if (fFolder) fFolder->Add(fTree);

  // Int_t n = fTree->GetEntriesFast();
  // Verbose(1, "Initialize", "A total of %d # entries in chain", n);
}

//____________________________________________________________________
void
Framework::ChainReader::Finish(Option_t* option)
{
  if (fTree) {
    Verbose(5, "Finish", "flushing chain %s after %d entries and %d bytes",
            fTree->GetName(), fEntry, fBytes);
    // delete fTree;
  }
  TreeReader::Finish(option);
}

//____________________________________________________________________
void
Framework::ChainReader::Print(Option_t* option) const 
{
  TreeReader::Print(option);
  TString opt(option);
  if (opt.Contains("F", TString::kIgnoreCase) && fTree) {
    std::cout << "Connected files: " << std::endl;
    TChain* chain = static_cast<TChain*>(fTree);
    TIter next(chain->GetListOfFiles());
    TChainElement* file = 0;
    while ((file = static_cast<TChainElement*>(next()))) 
      std::cout << "  " << file->GetTitle() << std::endl;
  }
}


//____________________________________________________________________
//
// EOF
//
