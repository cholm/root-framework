//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/ArrayIO.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:37 2004
    @brief   ArrayIO implementation file 
*/
#include "framework/ArrayIO.h"
#include "framework/DebugGuard.h"
#include <TROOT.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <iostream>


//====================================================================
// ClassImp(Framework::ArrayIO);

//____________________________________________________________________
Framework::ArrayIO::ArrayIO() 
  : Task("", ""),
    fTreeName(""), 
    fBranchName("")
{}
  
//____________________________________________________________________
Framework::ArrayIO::ArrayIO(const Char_t* name,  const Char_t* cName, 
			    const Char_t* bName, const Char_t* treeName)
  : Task(name, cName), 
    fTreeName(treeName), 
    fBranchName(bName)
{
  if (fBranchName.IsNull()) fBranchName = name;
  if (fTreeName.IsNull()) fTreeName = "T";
}


//____________________________________________________________________
void 
Framework::ArrayIO::Initialize(Option_t* option) 
{
  DEBUGGUARD(50,"Making TClonesArray of %s objects", GetTitle());
  if (!GetTitle() || GetTitle()[0] == '\0') {
    Fail("Initialize", "no class name given");
    return;
  }
  fCache = new TClonesArray(GetTitle());
  Framework::Task::Initialize(option);
}

//====================================================================
ClassImp(Framework::ArrayReader);

//____________________________________________________________________
Framework::ArrayReader::ArrayReader() 
  : ArrayIO("", "", "", "")
{}
  
//____________________________________________________________________
Framework::ArrayReader::ArrayReader(const Char_t* name, 
				    const Char_t* cName, 
				    const Char_t* bName, 
				    const Char_t* treeName)
  : ArrayIO(name, cName, bName, treeName)
{}

//____________________________________________________________________
void 
Framework::ArrayReader::Register(Option_t* option) 
{
  DEBUGGUARD(50,"Registering branch %s in tree %s holding %s objects", 
	     fBranchName.Data(), fTreeName.Data(), GetTitle());
  if (fTreeName.IsNull()) {
    Fail("Register", "no tree name set");
    return;
  }
  if (fBranchName.IsNull()) {
    Fail("Register", "no branch name set");
    return;
  }
  TTree* tree = 0;
  if (!(tree = GetBaseTree(fTreeName.Data()))) return;
  fBranch = tree->GetBranch(fBranchName.Data());
  if (!fBranch) {
    Fail("Register", "no input branch %s in tree %s", 
	 fBranchName.Data(), fTreeName.Data());
    return;
  }
  tree->SetBranchAddress(fBranchName.Data(), &fCache);
}

//====================================================================
ClassImp(Framework::ArrayWriter);

//____________________________________________________________________
Framework::ArrayWriter::ArrayWriter() 
  : ArrayIO("", "", "", "")
{}
  
//____________________________________________________________________
Framework::ArrayWriter::ArrayWriter(const Char_t* name, 
				    const Char_t* cName, 
				    const Char_t* bName, 
				    const Char_t* treeName)
  : ArrayIO(name, cName, bName, treeName)
{}

//____________________________________________________________________
void 
Framework::ArrayWriter::Register(Option_t* option) 
{
  DEBUGGUARD(50,"Registering branch %s in tree %s holding %s objects", 
	     fBranchName.Data(), fTreeName.Data(), GetTitle());
  if (fTreeName.IsNull()) {
    Fail("Register", "no tree name set");
    return;
  }
  if (fBranchName.IsNull()) {
    Fail("Register", "no branch name set");
    return;
  }
  TTree* tree = 0;
  if (!(tree = GetBaseTree(fTreeName.Data()))) return;
  fBranch = tree->Branch(fBranchName.Data(), &fCache);
  if (!fBranch) {
    Fail("Register", "couldn't make output branch %s in tree %s", 
	 fBranchName.Data(), fTreeName.Data());
    return;
  }
}

//____________________________________________________________________ 
//  
// EOF
//
