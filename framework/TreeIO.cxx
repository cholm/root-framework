
//____________________________________________________________________ 
//  
// $Id: TreeIO.cxx,v 1.9 2006-06-20 21:58:23 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/TreeIO.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:23:49 2004
    @brief   TreeIO Implementation file
*/
#include "framework/TreeIO.h"
#include "framework/Main.h"
#include "framework/DebugGuard.h"
#include <TDirectory.h>
#include <TROOT.h>
#include <TBrowser.h>
#include <TFolder.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <utility>
#include <string>
#include <vector>

//====================================================================
// ClassImp(Framework::TreeIO);

//____________________________________________________________________
Framework::TreeIO::TreeIO(const Char_t* name, const Char_t* title)
  : Task(name, title), 
    fFile(0),
    fTree(0),
    fBytes(0),
    fEntry(0)
{
  // Default constructor
}

//____________________________________________________________________
Framework::TreeIO::~TreeIO() 
{
  Finish();
}

//____________________________________________________________________
void 
Framework::TreeIO::Browse(TBrowser* b) 
{
  if (fFile)  b->Add(fFile);
  if (fTree)  b->Add(fTree);
}

//____________________________________________________________________
void 
Framework::TreeIO::Initialize(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  if (!GetTitle() || GetTitle()[0] == '\0') {
    Fail("Initialize", "no file name set");
    return;
  }
  fFile = TFile::Open(GetTitle(), option);
  if (!fFile || fFile->IsZombie()) {
    Fail("Initialize", "couldn't open file '%s' in mode '%s'", 
	 GetTitle(), option);
    return;
  }
  fFolder = Main::Instance()->GetFolder();
}

//____________________________________________________________________
void 
Framework::TreeIO::Finish(Option_t* option) 
{
  if (fTree && fFolder && Main::Instance()->GetFolder()) 
    Main::Instance()->GetFolder()->Remove(fTree);
  if (fFile)  {
    Verbose(10, "Finish", "closing file %s after %d entries and %d bytes", 
	    fFile->GetName(), fEntry, fBytes);
    fFile->Close();
  }
  fFile = 0;
  fTree = 0;
}

//____________________________________________________________________
void 
Framework::TreeIO::Print(Option_t* option) const 
{
  TString opt(option);
  Task::Print(option);
  TROOT::IndentLevel();
  std::cout << "  Entry #:      " << fEntry << std::endl;  
  TROOT::IndentLevel();
  std::cout << "  Bytes:        " << fBytes << std::endl;  
  if (fFile && opt.Contains("F", TString::kIgnoreCase)) {
    TROOT::IndentLevel();
    std::cout << "  File:         " << fFile->GetName() << std::endl;
  }
  if (fTree && opt.Contains("T", TString::kIgnoreCase)) {
    TROOT::IndentLevel();
    std::cout << "  Tree:         " << fTree->GetName() << std::endl;
  }
}

//====================================================================
ClassImp(Framework::TreeReader);

//____________________________________________________________________
Framework::TreeReader::TreeReader(const Char_t* name, const Char_t* title)
  : TreeIO(name, title) 
{}

//____________________________________________________________________
void 
Framework::TreeReader::Initialize(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  TreeIO::Initialize("READ");

  if (!GetName() || GetName()[0] == '\0') {
    Fail("Initialize", "no tree name set");
    return;
  }
  fTree = static_cast<TTree*>(fFile->Get(GetName()));
  if (!fTree) {
    Fail("Initialize", "couldn't get tree '%s'", GetName());
    return;
  }
  DEBUGMSG(50, "Adding TTree %s to folder %s", 
	   fTree->GetName(), fFolder->GetName());
  fFolder->Add(fTree);
}

//____________________________________________________________________
void 
Framework::TreeReader::Exec(Option_t* option) 
{
  Int_t read = fTree->GetEntry(fEntry);
  if (fEntry % 100 == 0)
    Verbose(5, "Exec", "read %d bytes @ entry # %d ", read, fEntry);
  else
    Verbose(10, "Exec", "read %d bytes @ entry # %d ", read, fEntry);
  if (read < 0) {
    Fail("Exec", "error while reading entry # %d from tree",
	 fEntry); 
    return;
  }
  if (read == 0) {
    Stop("Exec", "at end of tree after %d entries", fEntry);
    return;
  }
  fBytes += read;
  fEntry++;
}

//====================================================================
ClassImp(Framework::DelayedTreeReader);

//____________________________________________________________________
Framework::DelayedTreeReader::DelayedTreeReader(const Char_t* name, 
						const Char_t* tree, 
						const Char_t* file)
  : TreeReader(name, file), 
    fTreeName(tree),
    fSkip(1)
{
  // Default constructor
}

//____________________________________________________________________
void 
Framework::DelayedTreeReader::Initialize(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  // Should we make our own folder here?  I think so. 
  // fFolder = Main::Instance()->GetFolder();
  Framework::TreeIO::Initialize("READ");
  if (fTreeName.IsNull()) {
    Fail("Initialize", "no tree name set");
    return;
  }
  fTree = static_cast<TTree*>(fFile->Get(fTreeName.Data()));
  if (!fTree) {
    Fail("Initialize", "couldn't get tree '%s'", fTreeName.Data());
    return;
  }
  DEBUGMSG(50, "Adding TTree %s to folder %s", 
	   fTree->GetName(), fFolder->GetTitle());
  fFolder->Add(fTree);
}

//____________________________________________________________________
void 
Framework::DelayedTreeReader::Exec(Option_t* option) 
{
  Int_t entry = fEntry - fSkip;
  if (entry >= 0) {
    Int_t read = fTree->GetEntry(fEntry - fSkip);
    
    if (entry % 100 == 0)
      Verbose(5, "Exec", "read %d bytes @ entry # %d ", read, entry);
    else
      Verbose(10, "Exec", "read %d bytes @ entry # %d ", read, entry);
    
    if (read < 0) {
      Fail("Exec", "error while reading entry # %d from tree", entry); 
      return;
    }
    if (read == 0) {
      Verbose(1, "Exec", "at end of tree after %d entries", entry);
      return;
    }
    fBytes += read;
  }
  fEntry++;
}

//____________________________________________________________________
void 
Framework::DelayedTreeReader::Print(Option_t* option) const 
{
  // TString opt(option);
  TreeReader::Print(option);
  TROOT::IndentLevel();
  std::cout << "  Skip:         " << fSkip << std::endl;  
}

//====================================================================
ClassImp(Framework::TreeWriter);

//____________________________________________________________________
Framework::TreeWriter::TreeWriter(const Char_t* name, const Char_t* title) 
  : TreeIO(name, title) 
{}

//____________________________________________________________________
void 
Framework::TreeWriter::Initialize(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  TreeIO::Initialize("RECREATE");

  TDirectory* savdir = gDirectory;
  fFile->cd();

  if (!GetName() || GetName()[0] == '\0') {
    Fail("Initialize", "no tree name set");
    return;
  }
  fTree = new TTree(GetName(), GetName());
  if (!fTree) {
    Fail("Initialize", "couldn't make tree '%s'", GetName());
    savdir->cd();
    return;
  }
  DEBUGMSG(50, "Adding TTree %s to folder %s", 
	   fTree->GetName(), fFolder->GetName());
  fFolder->Add(fTree);
  savdir->cd();
}

//____________________________________________________________________
void 
Framework::TreeWriter::Exec(Option_t* option) 
{
  Int_t write = fTree->Fill();
  Verbose(15, "Exec", "filling %d bytes @ entry # %d ", write, fEntry);
  fBytes += write;
  fEntry++;
}

//____________________________________________________________________
void 
Framework::TreeWriter::Finish(Option_t* option) 
{
  if (fFile) {
    Verbose(10, "Finish", "flushing file %s after %d entries and %d bytes", 
	    fFile->GetName(), fEntry, fBytes);
    fFile->Write();
  }
  TreeIO::Finish(option);
}

//____________________________________________________________________ 
//  
// EOF
//
