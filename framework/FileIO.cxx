//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/FileIO.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:37 2004
    @brief   FileIO implementation file 
*/
#include "framework/FileIO.h"
#include "framework/DebugGuard.h"
#include <TFile.h>
#include <TList.h>
#include <iostream>


//====================================================================
// ClassImp(Framework::FileIO);

//____________________________________________________________________
Framework::FileIO::FileIO(const Char_t* name,  const Char_t* fileName, 
			  const Char_t* mode)
  : Task(name, fileName), 
    fFileName(fileName), 
    fMode(mode),
    fFile(0)
{
}

//____________________________________________________________________
void 
Framework::FileIO::Initialize(Option_t* option) 
{
  DEBUGGUARD(10,"Opening file %s in mode %s", fFileName.Data(), fMode.Data());
  fFile  = TFile::Open(fFileName, fMode);
  if (fFile) fFile->SetName("file");
  fCache = new TList;
  fCache->Add(fFile);
  Framework::Task::Initialize(option);
}
//____________________________________________________________________
void 
Framework::FileIO::Close()
{
  if (fFile) {
    fFile->SetName(fFileName);
    fFile->Close();
  }
  fFile = 0;
}

//====================================================================
ClassImp(Framework::FileReader);


//====================================================================
ClassImp(Framework::FileWriter);

//____________________________________________________________________
void 
Framework::FileWriter::Close()
{
  if (fFile) {
    Verbose(5,"Close", "Writing file to disk");
    fFile->Write();
    if (fDebug >= 5) fFile->ls();
  }
  FileIO::Close();
}

//____________________________________________________________________ 
//  
// EOF
//
