// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/Main.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:22:34 2004
    @brief   Main declaration file 
*/
#ifndef Framework_Main
#define Framework_Main
#include <framework/Task.h>
class TTree;
class TBrowser;

/** @namespace Framework Namespace for all %Framework classes */
namespace Framework 
{
  /** @defgroup fw_steer Steering and control classes. 
      @brief This group contains the steering and control classes. 
      
      @see Framework::Main 
  */
  /** @class Main framework/Main.h <Framework/Main.h>
      @ingroup fw_steer 
      @brief Task to manange all tasks.

      This is a singleton class (that is, "there can be only one" to
      quote a great movie from the 80'ies), and should be accessed via
      the method TFwMain::Instance(). The constructor can be used, but
      then it's the users responsibility to make sure it's only called
      once.
      
      Tasks and it's subclasses should be added directly or indirectly
      to the singleton.  The class has logic to handle errors in
      sub-tasks, so if a given Task sets the Task::Stop (or higher)
      message, the execution of the tasks will stop.  

      @p Execution Level

      Main uses the overloaded Task::Exec to initiate various
      execution levels.  The levels are: 
      <dl>
        <dt> Construction [Task constructor]: </dt>
	<dd>
          This is not handled by Task::Exec - rather this is when a
          given Task is constructed via the constructor. I mention it
          here for completeness only.
	</dd>
	<dt> Initialize [Task::Initialize(Option_t*)]:</dt>
	<dd>
          At this level, each Task will register it's TFolder in the
          Main's folder.  Task has logic to do this in a standard way,
          but if needed, it can be overridden in sub-classes.
	</dd>
        <dt> Register [Task::Register(Option_t*)]: </dt>
	<dd> 
          At this level, each Task must register the TBranch it will
          use in the appropiate TTree.  A pointer to a named tree can
	  be obtained via 
	  @code
	  TTree* tree = 0;
	  if (!(tree = GetBaseTree("T"))) return;
	  fBranch = tree->Branch(GetName(), &fCache);
	  @endcode 
        </dd>
        <dt>  Make [Task::Exec(Option_t*)]: </dt>
	<dd>
          This is the real task algorithm - at this level, a writer
          (see Task) will create data on it's TBranch and post it on
          it's TFolder for other Tasks to use.  A reader needn't do
          much (if anything) at this level.  If the input is a TFile
          with a TTree, it's enough for a reader to register it's
          TBranch and it's TCollection as it's TFolder for all the
          data to be visible to other tasks.  If the input is
          something other than a TTree, one may need to do something
          here (like read from a binary stream, and ASCII file, or the
          like).
	</dd>
	<dt> Finish [Task::Finish(Option_t*)]:</dt>
	<dd>
          At this level, each Task should do what it needs to do to
          finish off nicely, and it should remove it's TFolder from
          the base folder of Main.  There's logic in Task to do the
          later, but it can be overridden if needed.
	</dd>
      </dl>

      The steps are executed by Task::Exec, by passing it the
      appropiate option (please refer to Task::Exec).  Should new
      execution levels be needed, one can subclass Task, and override
      the Task::Exec method to accept additional options.

      @p Flow of Execution:

      The flow of execution is then:

      @verbatim 
                          +--------------+
                          | Construction |
                          +--------------+
                                 |
                           +------------+
                           | Initialize |
                           +------------+
                                 |
                            +----------+
                            | Register |
                            +----------+
                                 |
                 no     o-------------------o
                +---<---| fStatus < kStop ? |---<---+
                |       o-------------------o       |
                |                | yes              |
                |             +------+              |
                |             | Make |--------------+
                |             +------+
                |
                | 
           +--------+
           | Finish |
           +--------+
      @endverbatim 

      These steps, except the construction, are all executed by the
      Loop method.

      @p Usage Recomendation:

      A good way to use this framework, is to make a script that
      contains the construction of Tasks and the setting of parameters
      on these Task, and then execute that script, followed by the
      exection of Main::Instance()->Loop().  For example, one could
      have a script like:

      @dontinclude example/ReadWriteConfig.C 
      @skip {
      @until }

      And then one could execute all this with the following interpretor
      commands:
      @code 
        root[0] .x Config.C 
        root[1] Int retVal = Main::Instance()->Loop();
      @endcode 

      The program @link fwmain.cxx fwmain @endlink does just that.  It
      must be passed a script name that it will execute, and then it
      executes the Main::Instance()->Loop() method, returning the
      fStatus of the Main singleton.

      @c Subclassing this Class:

      If you need to change some behaviour of Main, you can simply
      sub-class this class.  However, you @e must make sure to
      explicitly construct the object, before doing anything else from
      this framework:
      
      @code 
        #ifndef __CINT__
        void Config() 
        #endif
        {
          gROOT->LoadClass("MyMain", "MyMain.so"); 
          gROOT->LoadClass("HitReader", "HitReader.so"); 
          gROOT->LoadClass("TrackWriter", "TrackWriter.so"); 
          MyMain*        main   = new MyMain::MyMain("main", "My Main");
          TreeReader*    input  = new TreeReader("input", "input.root"); 
          HitReader*     hits   = new HitReader("hits", "Hit reader");
          TrackWriter*   tracks = new TrackWriter("tracks", "Tracker");
          TreeWriter*    output = new TreeWriter("output", "output.root");
          main->Add(input);
          main->Add(hits);
          main->Add(tracks);
          main->Add(output);
        }
      @endcode

      Otherwise, you'll be using a normal Main object rather than an
      object of the sub-class. 
  */
  class Main : public Task
  {
  protected:
    /** Static instance of this class */
    static Main* fgInstance; 
  public:
    /** Default constructor - do not use */
    Main();
    /** Construct the singleton 
	@param name Name of top-level task
	@param title Title of top-level task  */
    Main(const Char_t* name, const Char_t* title="Main Task");
    /** Destruct this class */
    virtual ~Main() {}

    /**  Return a static instance of this object. It's initialised if
	 it doesn't already exits (defaule name is 'main', and title
	 is 'Main Task') */
    static   Main* Instance();

    /** The main loop.
	@param n Number of `events' - if negative, loop until status
	is set to kStop or larger 
	@return Status of the execution */
    virtual Int_t Loop(Int_t n=-1); //*MENU*
    
    /** Initialize the task and set the folder 
	@param option Not used. */
    virtual void Initialize(Option_t* option="");
    /** Does nothing.
	@param option Not used. */
    virtual void Register(Option_t* option="") {}

    /** Does nothing. 
	@param option Not used. */
    virtual void Exec(Option_t* option="") {}
    /** Deregister the folder 
	@param option Not used. */
    virtual void Finish(Option_t* option="");
    
    ClassDef(Main,1) // Main task to manage all tasks and jobs 
  };
}


#endif
//____________________________________________________________________ 
//  
// EOF
//
