// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id: DebugGuard.h,v 1.2 2007-08-05 09:25:48 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/DebugGuard.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:23:18 2004
    @brief   Debugguard declaration file 
*/
#ifndef Framework_DebugGuard
#define Framework_DebugGuard
#ifndef NDEBUG
# ifndef ROOT_TString
#  include <TString.h>
# endif

namespace Framework
{
  // Forward declaration 
  class Task;
  
  /** @defgroup fw_utils Utilities. */
  /** @class DebugGuard DebugGuard.h <framework/DebugGuard.h> 
      @brief A class to set up a debug guard. 
      @ingroup fw_utils 
  */
  class DebugGuard 
  {
  public:
    /** Constructor 
	@param client The client 
	@param lvl    The current debug level. 
	@param where  Location in code 
	@param format @c printf -like format string */
    DebugGuard(Task* client,
	       Int_t lvl,
	       const char* where, 
	       const char* format, ...);
    /** Destructor */
    ~DebugGuard();
    /** Show a message only  
	@param client The client 
	@param lvl    The current debug level. 
	@param where  Location in code 
	@param format @c printf -like format string */
    static void Message(Task* client, Int_t lvl, const char* where, 
			const char* format, ...);
  private:
    Task*   fClient;
    TString fMsg;
    TString fWhere;
    Int_t   fLvl;
  };
}
# ifdef __PRETTY_FUNCTION__
#  define DEBUGGUARD(lvl,format, ...) \
   Framework::DebugGuard g(this,lvl,__PRETTY_FUNCTION__,format,__VA_ARGS__)
#  define DEBUGMSG(lvl,format, ...) \
   Framework::DebugGuard::Message(this,lvl,__PRETTY_FUNCTION__, \
	                          format,__VA_ARGS__)
# else
#  define DEBUGGUARD(lvl,format, ...) \
   Framework::DebugGuard g(this,lvl,__FUNCTION__,format,__VA_ARGS__)
#  define DEBUGMSG(lvl,format, ...) \
   Framework::DebugGuard::Message(this,lvl,__FUNCTION__,format,__VA_ARGS__)
# endif
#else
# define DEBUGGUARD(lvl,format, ...) do { } while(false)
# define DEBUGMSG(lvl,format, ...) do { } while(false)
#endif
  
#endif
//
// EOF
//

  
  
