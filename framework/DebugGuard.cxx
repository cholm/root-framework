//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/DebugGuard.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:22:59 2004
    @brief   DebugGuard implementation file 
*/
#ifdef NDEBUG
namespace 
{  
 int intentionally_empty = 0;
}
#else
#include "framework/DebugGuard.h"
#include "framework/Task.h"
#include <iostream>
#include <iomanip>
#include <cstdarg>
#include <cstdio>

//____________________________________________________________________
Framework::DebugGuard::DebugGuard(Task* client,
				  Int_t lvl,
				  const char* where, 
				  const char* format, ...) 
  : fClient(client), fMsg(""), fWhere(where), fLvl(lvl)
{
  if (fClient->GetDebug() < fLvl) return;
  static char buffer[1024];
  va_list ap;
  va_start(ap,format);
  vsnprintf(buffer, 1024, format, ap);
  va_end(ap);
  fMsg = buffer;
  fClient->Debug(fLvl, fWhere.Data(), "=> %s", fMsg.Data());
}

//____________________________________________________________________
Framework::DebugGuard::~DebugGuard() 
{
  if (fClient->GetDebug() < fLvl) return;
  fClient->Debug(fLvl, fWhere.Data(), "<= %s", fMsg.Data());
}

//____________________________________________________________________
void
Framework::DebugGuard::Message(Task* client, Int_t lvl, const char* where, 
			       const char* format, ...) 
{
  if (client->GetDebug() < lvl) return;
  static char buffer[1024];
  va_list ap;
  va_start(ap,format);
  vsnprintf(buffer, 1024, format, ap);
  va_end(ap);
  client->Debug(lvl, where, "== %s", buffer);
}
#endif

//____________________________________________________________________
//
// EOF
//

  
