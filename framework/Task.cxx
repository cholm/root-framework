//____________________________________________________________________ 
//  
// $Id: Task.cxx,v 1.12 2007-08-05 09:25:48 cholm Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/Task.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:22:59 2004
    @brief   Task implementation file 
*/
#include <TFolder.h>
#include "TBranch.h"
#include <TTree.h>
#include <TClass.h>
#include <TCollection.h>
#include <TBrowser.h>
#include <TString.h>
#include <TROOT.h>
#include <TDirectory.h>
#include <TClonesArray.h>
#include "framework/DebugGuard.h"
#include "framework/Task.h"
#include "framework/Main.h"
#include <iostream>
    
//____________________________________________________________________
// ClassImp(Framework::Task);

//____________________________________________________________________
Framework::Task::Task(const Char_t* name, const Char_t* title)
  : TTask(name, title), 
    fParent(0), 
    fBranch(0), 
    fFolder(0), 
    fVerbose(0),
    fDebug(0), 
    fCache(0)
{
  // Default constructor
  SetStatus();
}

//____________________________________________________________________
TTree*
Framework::Task::GetBaseTree(const Char_t* name)
{
  // Get the tree from the main module - deactivate if it isn't
  // possible. Return 0 on error
  DEBUGGUARD(50, "Get tree %s", name);
  if (!name || name[0] == '\0') return 0;
  TFolder* folder = 0;
  if (!(folder = GetBaseFolder())) return 0;

  TString path(folder->FindFullPathName(name));
  if (path.IsNull()) {
    Fail("GetBaseTree", "could not find tree '%s' in '%s'", 
	 name, folder->GetName());
    return 0;
  }

  if (path[0] == '/' && path[1] == '/' && !path.BeginsWith("//root"))
    path.Insert(1, "/root");

  TObject* found = folder->FindObject(path.Data());
  if (!found) {
    Fail("GetBaseTree", "couldn't get the object '%s'", path.Data());
    return 0;
  }

  if (!(found->IsA()->InheritsFrom(TTree::Class()))) {
    Fail("GetBaseTree", "object found at '%s' in not a TTree but a %s", 
	 path.Data(), found->ClassName());
    return 0;
  }
  TTree* tree = static_cast<TTree*>(found);
  return tree;
}

//____________________________________________________________________
TFolder*
Framework::Task::GetBaseFolder() 
{
  // Get the folder from the main module - deactivate if it isn't
  // possible. Return 0 on error
  DEBUGGUARD(50, "Get top-level folder%c", ' ');
  TFolder* folder = fFolder;
  if (fParent) 
    folder = fParent->GetBaseFolder();

  if (!(folder = Main::Instance()->GetFolder())) 
    Fail("Initialize", "couldn't get main folder");

  return folder;
}

//____________________________________________________________________
void
Framework::Task::Browse(TBrowser* b)
{
  DEBUGGUARD(50, "Browse task with %p", b);
  b->Add(fTasks, "Tasks");
  if      (fCache)    b->Add(fCache, "Cache");
  else if (fFolder)   b->Add(fFolder, "Folder");
  if      (fBranch)   b->Add(fBranch);
}

//____________________________________________________________________
void
Framework::Task::Add(TTask* task) 
{
  DEBUGGUARD(50, "Adding a task %p", task);
  if (!task) return;
  // Framework::Task* ftask = dynamic_cast<Framework::Task*>(task);
  Framework::Task* ftask = (task->IsA()->
			    InheritsFrom(Framework::Task::Class()) ? 
			    static_cast<Framework::Task*>(task) : 0);
  if (!ftask) {
    Stop("Add", "task %s (0x%X) does not inherit from Framework::Task",
	 task->GetName(), task);
      return;
  }
  if (ftask->fParent) {
    ftask->Fail("Add", "Task %s already has a parent %s (0x%X)",
		ftask->GetName(), ftask->fParent->GetName(), ftask->fParent);
    return;
  }
  TTask::Add(ftask);
  ftask->fParent = this;
}


//____________________________________________________________________
Bool_t
Framework::Task::PreExec(const Char_t* mName, Option_t* option)
{
  DEBUGGUARD(50, "Before executing %s", mName);
  if (fgBeginTask) {
    Error(mName, "can not execute task %s, already running task %s",
	  GetName(), fgBeginTask->GetName());
    return kFALSE;
  }
  if (!IsActive()) {
    DEBUGMSG(40, "task %s isn't active, not executing %s", GetName(), mName);
    return kFALSE;
  }
  fOption = option;
  fgBeginTask = this;
  fgBreakPoint = 0;
  if (fBreakin) return kFALSE;

  DEBUGMSG(60, "executing %s->%s(\"%s\")", GetName(), mName, option);

  if (fDebug || fVerbose) TROOT::IncreaseDirLevel();
  return kTRUE;
}

//____________________________________________________________________
Bool_t
Framework::Task::PostExec()
{
  DEBUGGUARD(50, "After executing a member function%c", ' ');
  fHasExecuted = kTRUE;
  if (fDebug || fVerbose) TROOT::DecreaseDirLevel();
  if (fBreakout) return kFALSE;
  if (!fgBreakPoint) {
    fgBeginTask->CleanTasks();
    fgBeginTask = 0;
  }
  return kTRUE;
}

//____________________________________________________________________
void
Framework::Task::SubExec(const char* name, 
			 void (Task::*func)(Option_t*), Option_t* option) 
{
  TIter next(fTasks);
  Task *task;
  while((task = static_cast<Task*>(next()))) {
    // Check if there's a break point here 
    if (fgBreakPoint) return;

    // Check if task is active. 
    if (!task->IsActive()) continue;

    // Check if we have executed
    if (!task->fHasExecuted) {
      if (task->fBreakin == 1) {
	DEBUGMSG(10, "Break at entry of task: %s",task->GetName());
	fgBreakPoint = this; 
	task->fBreakin++; 
	return; 
      }
      DEBUGMSG(60, "executing task %s->%s(\"%s\")", 
	       task->GetName(), name, option);
      if (fDebug > 1 || fVerbose > 1) TROOT::IncreaseDirLevel();
      (task->*func)(option);  
      task->fHasExecuted = kTRUE;
    }

    // Execute sub-tasks 
    task->SubExec(name, func,option);

    //
    if (fDebug > 1 || fVerbose > 1) TROOT::DecreaseDirLevel();

    // Check if we should break on end of function 
    if (task->fBreakout == 1) {
      DEBUGMSG(10, "Break at exit of task: %s", task->GetName());
      fgBreakPoint = this;
      task->fBreakout++;
      return;
    }

    // Check status 
    if (task->fStatus >= kStop) {
      DEBUGMSG(40, "Task %s set status to %d>=kStop, breaking out", 
	       task->GetName(), fStatus);
      // We clear the flag in case the error wasn't fatal
      if (task->fStatus < kFail) task->fStatus = kOk;
      return;
    }
  }
}

//____________________________________________________________________
void
Framework::Task::ExecInitialize(Option_t* option) 
{
  if (!PreExec("Initialize", option)) return;
  Initialize(option);
  SubExec("Initialize", &Task::Initialize,option);
  PostExec();
}

//____________________________________________________________________
void
Framework::Task::Initialize(Option_t* option) 
{
  // Register the folder 
  // Options:
  // 
  //   None 
  // 
  TFolder* folder = fFolder;
  if (fParent) folder = fParent->fFolder;
  if (!folder) folder = GetBaseFolder();
  if (!folder) return;
  DEBUGMSG(35,"Making folder %s in folder %s with collection 0x%X (%s)",
	   GetName(), folder->GetName(), fCache, 
	(fCache ? fCache->ClassName() : "none"));
  fFolder = folder->AddFolder(GetName(), GetTitle(), fCache); 
}

//____________________________________________________________________
void
Framework::Task::ExecRegister(Option_t* option) 
{
  if (!PreExec("Register", option)) return;
  Register(option);
  SubExec("Register", &Task::Register,option);
  PostExec();
}

//____________________________________________________________________
void
Framework::Task::ExecuteTask(Option_t* option) 
{
  if (!PreExec("Exec", option)) return;
  Exec(option);
  SubExec("Exec", &Task::Exec, option);
  PostExec();
}

//____________________________________________________________________
void
Framework::Task::ExecFinish(Option_t* option) 
{
  if (!PreExec("Finish", option)) return;
  Finish(option);
  SubExec("Finish", &Task::Finish,option);
  PostExec();
}

//____________________________________________________________________
void
Framework::Task::Finish(Option_t* option) 
{
  // De-register the folder 
  // Options:
  // 
  //   None 
  // 
  if (fFolder) {
    DEBUGMSG(50, "Removing folder %s from parent", GetName());
    if (fParent && fParent->GetFolder()) 
      fParent->GetFolder()->Remove(fFolder);
    else if (Main::Instance()->GetFolder()) 
      Main::Instance()->GetFolder()->Remove(fFolder);
  }
  fFolder = 0;
  fBranch = 0;
}

//____________________________________________________________________
void
Framework::Task::ExecReset(Option_t* option) 
{
  if (!PreExec("Reset", option)) return;
  Reset(option);
  SubExec("Reset", &Task::Reset, option);
  PostExec();
}

//____________________________________________________________________
void 
Framework::Task::Stop(const Char_t* w, const Char_t* f, ...) 
{
  // Set status to kStop and flag Main to stop (set the fStatus of
  // that to kStop too).  Task is still active
  SetStatus(kStop);
  Main::Instance()->SetStatus(kStop);
  va_list ap;
  va_start(ap, f);
  for (Int_t i = 0; i < gROOT->GetDirLevel(); i++) std::cerr.put(' ');
  DoError(kStop, w, f, ap);
  va_end(ap);
}

//____________________________________________________________________
void
Framework::Task::Fail(const Char_t* w, const Char_t* f, ...) 
{
  // Set status to kFail and flag Main to fail (set the fStatus of
  // that to kFail too). Task is deactivated
  SetActive(kFALSE);
  SetStatus(kFail);
  Main::Instance()->SetStatus(kFail);
  va_list ap;
  va_start(ap, f);
  for (Int_t i = 0; i < gROOT->GetDirLevel(); i++) std::cerr.put(' ');
  DoError(kFail, w, f, ap);
  va_end(ap);
}

//____________________________________________________________________
void
Framework::Task::Abort(const Char_t* w, const Char_t* f, ...) 
{
  // Set status to kAbort and flag Main to abort (set the fStatus of
  // that to kAbort too). Task is deactivated
  SetStatus(kAbort);
  SetActive(kFALSE);
  Main::Instance()->SetStatus(kAbort);
  va_list ap;
  va_start(ap, f);
  for (Int_t i = 0; i < gROOT->GetDirLevel(); i++) std::cerr.put(' ');
  DoError(kAbort, w, f, ap);
  va_end(ap);
}
//____________________________________________________________________
void
Framework::Task::Debug(Int_t lvl, const Char_t* w, const Char_t* f, ...) const
{
  // Print a message conditional on the fDebug member: 
  // if fDebug >= level the message is printed
  if (fDebug < lvl) 
    return;
  va_list ap;
  va_start(ap, f);
  for (Int_t i = 0; i < gROOT->GetDirLevel(); i++) std::cerr.put(' ');
  DoError(kInfo+200, w, f, ap);
  va_end(ap);
}

//____________________________________________________________________
void
Framework::Task::Verbose(Int_t lvl, const Char_t* w, const Char_t* f, ...) const
{
  // Print a message conditional on the fVerbose member: 
  // if fVerbose >= level the message is printed
  if (fVerbose < lvl) 
    return;
  va_list ap;
  va_start(ap, f);
  // TROOT::IndentLevel();
  for (Int_t i = 0; i < gROOT->GetDirLevel(); i++) std::cerr.put(' ');
  DoError(kInfo+100, w, f, ap);
  va_end(ap);
}

//____________________________________________________________________
void
Framework::Task::SetVerbose(Int_t v) 
{
  Verbose(50, "SetVerbose", "setting verbosity of %s to %d", GetName(), v);
  fVerbose = v;
  TIter next(fTasks);
  Task* task;
  TROOT::IncreaseDirLevel();
  while ((task = static_cast<Task*>(next())))
    task->SetVerbose(v);
  TROOT::DecreaseDirLevel();
}

//____________________________________________________________________
void
Framework::Task::SetDebug(Int_t d) 
{
  DEBUGGUARD(50, "setting debug level of %s to %d", GetName(), d);
  fDebug = d;
  TIter next(fTasks);
  Task* task = 0;
  TROOT::IncreaseDirLevel();
  while ((task = static_cast<Task*>(next()))) 
    task->SetDebug(d);
  TROOT::DecreaseDirLevel();
}

//____________________________________________________________________
void
Framework::Task::Print(Option_t* option) const
{
  // Print information on this module 
  // Options:
  // 
  //   D        Print details
  //   R        Print recursive 
  //
  TString opt(option);
  TROOT::IndentLevel();
  for (Int_t i = TROOT::GetDirLevel(); i < 80; i++) 
    std::cout << "-";
  std::cout << std::endl;

  TROOT::IndentLevel();
  std::cout << ClassName() << ": " << GetName() << std::endl;
  if (opt.Contains("D", TString::kIgnoreCase)) {
    TROOT::IndentLevel();
    std::cout << "==> " << GetTitle() << " <==" << std::endl;
    TROOT::IndentLevel();
    std::cout << "  Verbose:      " << fVerbose << std::endl; 
    TROOT::IndentLevel();
    std::cout << "  Debug:        " << fDebug   << std::endl; 
    if (fFolder) {
      TROOT::IndentLevel();
      std::cout << "  Folder:       " << fFolder->GetName() << std::endl;
    }
    if (fBranch) {
      TROOT::IndentLevel();    
      std::cout << "  Branch:       " << fBranch->GetName() << std::endl;
    }
    if (fCache) {
      TROOT::IndentLevel();    
      std::cout << "  Cache:        " << fCache->GetName() << std::endl;
    }
  }
  TROOT::IndentLevel();
  for (Int_t i = TROOT::GetDirLevel(); i < 80; i++) 
    std::cout << "-";
  std::cout << std::endl;

  if (!opt.Contains("R", TString::kIgnoreCase)) return;
  
  Ssiz_t n = opt.Index("R");
  if (n+1 < opt.Length()) {
    Char_t c = opt[n+1];
    switch (c) {
    case '1': opt.Remove(n,2); break;
    case '2': opt.Replace(n+1,1,"1"); break;
    case '3': opt.Replace(n+1,1,"2"); break;
    case '4': opt.Replace(n+1,1,"3"); break;
    case '5': opt.Replace(n+1,1,"4"); break;
    case '6': opt.Replace(n+1,1,"5"); break;
    case '7': opt.Replace(n+1,1,"6"); break;
    case '8': opt.Replace(n+1,1,"7"); break;
    case '9': opt.Replace(n+1,1,"8"); break;
    }
  }
  TIter next(fTasks);
  Task *task;
  TROOT::IncreaseDirLevel();
  while((task = static_cast<Task*>(next()))) 
    task->Print(opt.Data());
  TROOT::DecreaseDirLevel();
}

//____________________________________________________________________ 
TDirectory*
Framework::Task::FindDirectory(const char* path)
{
  TFolder* base = GetBaseFolder();
  TObject* of   = base->FindObject(path);
  if (!of)                                    return 0;
  if (!of->InheritsFrom(TDirectory::Class())) return 0;
  return static_cast<TDirectory*>(of);
}
  
//____________________________________________________________________ 
void
Framework::Task::WriteFolder(TDirectory* top, bool recurse, TFolder* folder)
{
  if (!folder) folder = fFolder;
  if (!folder)                                       return;
  TCollection* collection = folder->GetListOfFolders();
  if (!collection or collection->GetEntries() <= 0)  return;

  TDirectory* d = top->mkdir(GetName(),GetTitle(),true);
  d->cd();
  for (auto o : *collection) {
    if (o->InheritsFrom(TFolder::Class())) {
      if (recurse) WriteFolder(d, recurse, static_cast<TFolder*>(o));
      continue;
    }
    d->WriteTObject(o,o->GetName(),"",TObject::kSingleKey);
  }
  top->cd();
};
//____________________________________________________________________ 
//  
// EOF
//
