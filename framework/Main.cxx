//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/Main.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:22:12 2004
    @brief   Main implementation file 
*/
#include "framework/Main.h"
#include "framework/DebugGuard.h"
#include <TDirectory.h>
#include <TROOT.h>
#include <TBrowser.h>
#include <TTree.h>
#include <TFolder.h>
#include <iostream>


//____________________________________________________________________
// ClassImp(Framework::Main);

//____________________________________________________________________
Framework::Main* Framework::Main::fgInstance = 0;

//____________________________________________________________________
Framework::Main::Main()
{
  if (fgInstance) {
    Warning("Main", "Framework::Main is already instantized");    
    if (fgInstance->fFolder) {
      Warning("Main", "Putting retrivied object in singletons folder");
      fgInstance->fFolder->Add(this);
    }
    return;
  }
  fgInstance = this;
  gROOT->GetListOfBrowsables()->Add(fgInstance);
}

//____________________________________________________________________
Framework::Main::Main(const Char_t* name, const Char_t* title) 
  : Task(name, title)
{
  // Defualt ctor
  if (fgInstance) {
    Abort("Main", "only one instance of Framework::Main");
    return;
  } 
  fgInstance = this;
  gROOT->GetListOfBrowsables()->Add(fgInstance);
}

//____________________________________________________________________
Framework::Main* 
Framework::Main::Instance() 
{
  // Return a static instance of this object. It's initialised if it
  // doesn't already exits (defaule name is 'main', and title is
  // 'Top-most task')
  if (!fgInstance) fgInstance = new Main("main");
  return fgInstance;
}


//____________________________________________________________________
Int_t 
Framework::Main::Loop(Int_t n) 
{
  // The main loop
  if (fgInstance != this) { 
    SetActive(kFALSE);
    fStatus = kStop;
  } 
  if (fStatus >= kStop) {
    Verbose(1, "Loop", "This main task isn't active - "
	    "probably you have already instantized a main task");
    if (fgInstance != this)
      Verbose(1, "Loop", "The main task instance is %s (0x%X)", 
	      fgInstance->GetName(), fgInstance);
    return fStatus;
  }
  ExecInitialize("");
  
  if (fStatus < kStop) {
    ExecRegister("");
    
    // if (fStatus < kStop) ExecuteTask("print");
    
    Int_t i = 0;
    while (fStatus < kStop) {
      if (n > 0 && i >= n) break;
      ExecuteTask("");
      i++;
    }
    if (fStatus < kFail) ExecFinish();
  }
  return (fStatus == kStop ? 0 : fStatus);
}


//____________________________________________________________________
void 
Framework::Main::Initialize(Option_t* option) 
{
  // Set the folder 
  DEBUGGUARD(50, "Initializing main manager called %s", GetName());
  if (fgInstance != this) {
    SetActive(kFALSE);
    fStatus = kStop;
  }
  else {
    DEBUGMSG(50, "Making folder for main manager called %s/%s", 
	     GetName(), GetTitle());
    if (!gROOT->GetRootFolder()) {
      Error("Initialize", "No ROOT folder!");
      fStatus = kStop;
      return;
    }
    fFolder = gROOT->GetRootFolder()->AddFolder(GetName(), GetTitle());
  }
  if (fVerbose > 1) 
    Print(fVerbose > 2 ? "DR" : "R");
}

//____________________________________________________________________
void 
Framework::Main::Finish(Option_t* option) 
{
  // De-register the folder 
  // Options:
  // 
  //   None 
  // 
  // gROOT->GetRootFolder()->Remove(fFolder);
  // fFolder = 0;
}



//____________________________________________________________________ 
//  
// EOF
//
