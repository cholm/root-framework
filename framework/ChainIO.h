// -*- mode: C++ -*- 
//
//____________________________________________________________________ 
//
//  ROOT generic framework
//  Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/ChainIO.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Dec 22 18:30:27 2004
    @brief   Declaration of ChainIO
*/
#ifndef Framework_ChainIO
#define Framework_ChainIO
#include <framework/TreeIO.h>
#include <TObjArray.h>

class TChain;
class TTree;
class TFile;
class TIter;
class TBrowser;

namespace  Framework 
{
  //__________________________________________________________________
  /** @class ChainWriter framework/ChainIO.h <framework/ChainIO.h>
      @ingroup fw_special_tasks
      @brief Task to write a TTree set of files (a chain) holding the
      same branches. 

      The user can specify to break the chain into files based on the
      number of entries or the number of bytes writen to disk. 

      The user can explicitly specify which files to write to, or can
      provide a template as the second argument to the contructor.
      The template should contain the sub-string @c XXXXXX - these
      will be replaced by the file number.  For example 
      @code  
      Framework::ChainWriter* writer = 
	new Framework::ChainWriter("Tree", 
	                           "root://localhost/scratch/fileXXXXXX.root");
      writer->SetMaxEntries(1000);				   
      @endcode 
  */
  class ChainWriter : public TreeWriter
  {
  protected:
    /** list of specified files, if any */
    TObjArray* fFiles;
    /** Iterator over list of files */
    TIter* fNextFile;
    /** The current file number */
    Int_t fFileNumber;
    /** The maxium number of bytes to write to a file */
    ULong_t fMaxBytes;
    /** The maximum number of enries to write to a file */
    ULong_t fMaxEntries;
    /** Check for a more files to open, make a tree in the new file
	(if any), and close the previous file */
    void NewFile();
  public:
    /** Make a new chain writer.
	@param chainName Name of the chain (tree) to make in the
	files. 
	@param title Pattern of automatic file names.  The pattern
	should contain the substring @c XXXXXX if automatic file names
	are to be used.   If the user specifies specific file names
	via the member function AddFile, this argument can be
	anything.  */
    ChainWriter(const Char_t* chainName="T", 
		const Char_t* title="fileXXXXXX.root");
    /** Destructor */
    virtual ~ChainWriter();
    /** Add a file name to the list of files to write to 
	@param filename File name to write to  */
    virtual void AddFile(const Char_t* filename);
    /** Set the maximum number of bytes that should be writen to a
	single file.  If the argument is zero then the number of bytes
	written isn't watched. 
	@param max Maximum number of bytes to write to a single file  */
    virtual void SetMaxBytes(ULong_t max=0) { fMaxBytes = max; }
    /** Set the maximum number of entries that should be writen to a
	single file.  If the argument is zero then the number of entries
	written isn't watched. 
	@param max Maximum number of entries to write to a single file  */
    virtual void SetMaxEntries(ULong_t max=0) { fMaxEntries = max; }
    /** Initialize the task.   Opens the first file and makes the tree
	in that file. 
	@param option Not used. */
    virtual void Initialize(Option_t* option="");
    /** Write next entry to file.  If the maximum number of bytes
	and/or entries is exceeded, a new file is opened. 
	@param option Not used.  */
    virtual void Exec(Option_t* option="");
    /** Finish off.  Flushes and closes the current file, and resets
	parameters. 
	@param option  Not used.  */
    virtual void Finish(Option_t* option="");
    /** Print information on this task. 
	@param option  Options  */
    virtual void Print(Option_t* option="") const;
    /** Browse this task. 
	@param b Browser to use  */
    virtual void Browse(TBrowser* b);
    
    ClassDef(ChainWriter,1);
  };

  //__________________________________________________________________
  /** @class ChainReader framework/ChainIO.h <framework/ChainIO.h>
      @ingroup fw_special_tasks
      @brief Task to read a TTree set from files (a chain) holding the
      same branches. 

      The user can explicitly specify which files to read from, or can 
      provide a template as the second argument to the contructor.
      The template should contain the sub-string @c XXXXXX - these
      will be replaced by the file number as they exists in the input
      directory.  For example 
      @code  
      Framework::ChainReader* reader = 
	new Framework::ChainReader("Tree", 
	                           "root://localhost/scratch/fileXXXXXX.root");
      @endcode 
  */
  class ChainReader : public TreeReader
  {
  protected:
  public:
    /** Make a new chain writer.
	@param chainName Name of the chain (tree) to read from the
	files. 
	@param title Pattern of automatic file names.  The pattern
	should contain the substring @c XXXXXX if automatic file names
	are to be used.   If the user specifies specific file names
	via the member function AddFile, this argument can be
	anything.  */
    ChainReader(const Char_t* chainName, const Char_t* title);
    /** Destructor */
    virtual ~ChainReader() {}
    /** Add a file name to the list of files to read from
	@param filename Name of file to read from  */
    virtual void AddFile(const Char_t* filename);
    /** Add files listed in input file.  Blank lines and lines
	starting with '#' are ignored.  File names should end in
	".root"
	@param listName Name of file to read ROOT file names from */
    virtual void AddFiles(const Char_t* listName);
    /** Initialize the task.   Makes the chain and adds files to that
	@param option Not used. */
    virtual void Initialize(Option_t* option="");
    /** Finish off.  Flushes and closes the current file, and resets
	parameters. 
	@param option  Not used.  */
    virtual void Finish(Option_t* option="");
    /** Print information on this task. 
	@param option  Options  */
    virtual void Print(Option_t* option="") const;
    
    ClassDef(ChainReader,1);
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
