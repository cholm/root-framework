//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/fwmain.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:24:39 2004
    @brief   Implementation of program @c fwmain
*/
#ifndef __CINT__
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
# include <TROOT.h>
# include <TApplication.h>
# include <TInterpreter.h>
# include <TSystem.h>
# include <TString.h>
# include <TObjString.h>
# include <TList.h>
# include <iostream>
# include <sstream>
# include <cstdlib>
# include "framework/Main.h"
#endif

void load(const TList& mods, Int_t debug)
{
  TIter next(&mods);
  TObjString* m;
  while ((m = static_cast<TObjString*>(next()))) {
    if (debug > 0)
      std::cout << "Loading " << m->GetName() << " ..." << std::flush;
    const TString& s = m->GetString();
    if (s.EndsWith(".C")) 
      gROOT->Macro(s+"+s");
    else
      gSystem->Load(m->GetName());
    if (debug > 0) std::cout << "done" << std::endl;
  }
}

int loop(const TList& scrs, Int_t n=-1, Int_t verbose=0, Int_t debug=0) 
{
  for (auto s : scrs) {
    Int_t error;
    const char* scr = s->GetName();
    gROOT->Macro(scr, &error);
    if (error != TInterpreter::kNoError) {
      std::cerr << "Execution of " << scr << " failed" << std::endl;
      return 0;
    }
  }
  Framework::Main* mainTask = Framework::Main::Instance();
  mainTask->SetVerbose(verbose);
  mainTask->SetDebug(debug);
  return mainTask->Loop(n);
}

#ifndef __CINT__
void usage(const char* name) 
{
  std::cout << "Usage: " << gSystem->BaseName(name) << " [OPTIONS]\n\n"
	    << "Options:\n"
	    << "\t-h\tThis help\n"
	    << "\t-b\tRun in batch mode\n"
	    << "\t-v NUM\tSet verbosity to NUM (default 0)\n"
	    << "\t-d NUM\tSet debug level to NUM (default 0)\n"
	    << "\t-n NUM\tSet number of events to NUM (default -1)\n"
	    << "\t-l LIB\tLoad library before executing config\n"
	    << "\tSCRIPTS\tConfiguration scripts to use (default Config.C)\n"
	    << std::endl;
}

void version(const char* name) 
{
  std::cout << PACKAGE_STRING << "\n" 
	    << "Copyright (c) 2004 Christian Holm Christensen " 
	    << PACKAGE_BUGREPORT << "\n"
	    << std::endl;
}

template <typename T> 
T get_arg(char o, const char* arg) 
{
  if (!arg) {
    std::cerr << "Option '-" << o << "' needs an argument"  << std::endl;
    exit (1);
  }
  std::stringstream s(arg);
  T retval = T();
  s >> retval;
  return retval;
}

int main(int argc, char** argv)
{
  Int_t   verbose = 0;
  Int_t   debug   = 0;
  Int_t   n       = -1;
  Bool_t  batch   = kFALSE;
  TList   mods;
  TList   scrs;
  for (Int_t i = 1; i < argc; i++) {
    if (argv[i] && argv[i][0] != '-') {
      scrs.Add(new TObjString(argv[i]));
      continue;
    }
    switch (argv[i][1]) {
    case 'h': usage(argv[0]); return 0;
    case 'v': verbose = get_arg<Int_t>('v', argv[++i]); break;
    case 'V': version(argv[0]);  break;
    case 'd': debug   = get_arg<Int_t>('d', argv[++i]); break;
    case 'b': batch   = kTRUE; break;
    case 'n': n       = get_arg<Int_t>('n', argv[++i]); break;
    case 'l': mods.Add(new TObjString(get_arg<TString>('l',argv[++i])));
      break;
    default:
      std::cerr << "Unknown option " << argv[i] << " try " 
		<< gSystem->BaseName(argv[0]) << " -h" 
		<< std::endl;
      return 1;
    }
  }
  if (debug > 0) {
    std::cout << argv[0] << " was called with the following arguments:\n"
	      << "\tverbose:\t" << verbose << "\n"
	      << "\tdebug:\t\t" << debug << "\n"
	      << "\tevents:\t\t" << n << "\n"
	      << "\tbatch:\t\t" << (batch ? "yes" : "no") 
	      << std::endl;
  }
  TApplication app("app", 0, 0);
  gROOT->SetBatch(batch);

  if (scrs.GetEntries() <= 0) scrs.Add(new TObjString("Config.C"));
  
  load(mods,debug);
  return loop(scrs, n, verbose, debug);
}
#endif

//____________________________________________________________________ 
//  
// EOF
//
