//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/ObjectIO.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:37 2004
    @brief   ObjectIO implementation file 
*/
#include "framework/ObjectIO.h"
#include "framework/DebugGuard.h"
#include <TClass.h>
#include <TROOT.h>
#include <TTree.h>
#include <TFolder.h>
#include <iostream>

//====================================================================
// ClassImp(Framework::ObjectIO);

//____________________________________________________________________
Framework::ObjectIO::ObjectIO() 
  : Task("", ""),
    fTreeName(""), 
    fBranchName(""),
    fObject(0)
{}
  
//____________________________________________________________________
Framework::ObjectIO::ObjectIO(const Char_t* name,  const Char_t* cName, 
			      const Char_t* bName, const Char_t* treeName)
  : Task(name, cName), 
    fTreeName(treeName), 
    fBranchName(bName),
    fObject(0)
{
  if (fBranchName.IsNull()) fBranchName = name;
  if (fTreeName.IsNull())   fTreeName = "T";
}


//____________________________________________________________________
void 
Framework::ObjectIO::Initialize(Option_t* option) 
{
  if (!fObject) {
    if (!GetTitle() || GetTitle()[0] == '\0') {
      Fail("Initialize", "no class name given");
      return;
    }
    Warning("Initialize", "making a default %s object", GetTitle());
    TClass* cl = gROOT->GetClass(GetTitle());
    fObject    = static_cast<TObject*>(cl->New());
  }
  Framework::Task::Initialize(option);
  if (fFolder) fFolder->Add(fObject);
}

//====================================================================
ClassImp(Framework::ObjectReader);

//____________________________________________________________________
Framework::ObjectReader::ObjectReader() 
  : ObjectIO("", "", "", "")
{}
  
//____________________________________________________________________
Framework::ObjectReader::ObjectReader(const Char_t* name, 
				      const Char_t* cName, 
				      const Char_t* bName, 
				      const Char_t* treeName)
  : ObjectIO(name, cName, bName, treeName)
{}

//____________________________________________________________________
void 
Framework::ObjectReader::Register(Option_t* option) 
{
  DEBUGGUARD(50,"Registering branch %s in tree %s holding %s objects", 
	     fBranchName.Data(), fTreeName.Data(), GetTitle());
  if (fTreeName.IsNull()) {
    Fail("Register", "no tree name set");
    return;
  }
  if (fBranchName.IsNull()) {
    Fail("Register", "no branch name set");
    return;
  }
  TTree* tree = 0;
  if (!(tree = GetBaseTree(fTreeName.Data()))) return;
  fBranch = tree->GetBranch(fBranchName.Data());
  if (!fBranch) {
    Fail("Register", "no input branch %s in tree %s", 
	 fBranchName.Data(), fTreeName.Data());
    return;
  }
  if (!fObject) {
    Fail("Register", "object not initialized to point to a %s object", 
	 GetTitle());
    return;
  }
  tree->SetBranchAddress(fBranchName.Data(), &fObject);
  // fBranch->SetAddress(&fCache);
}

//====================================================================
ClassImp(Framework::ObjectWriter);

//____________________________________________________________________
Framework::ObjectWriter::ObjectWriter() 
  : ObjectIO("", "", "", "")
{}
  
//____________________________________________________________________
Framework::ObjectWriter::ObjectWriter(const Char_t* name, 
				      const Char_t* cName, 
				      const Char_t* bName, 
				      const Char_t* treeName)
  : ObjectIO(name, cName, bName, treeName)
{}

//____________________________________________________________________
void 
Framework::ObjectWriter::Register(Option_t* option) 
{
  DEBUGGUARD(50,"Registering branch %s in tree %s holding %s objects", 
	fBranchName.Data(), fTreeName.Data(), GetTitle());
  if (fTreeName.IsNull()) {
    Fail("Register", "no tree name set");
    return;
  }
  if (fBranchName.IsNull()) {
    Fail("Register", "no branch name set");
    return;
  }
  TTree* tree = 0;
  if (!(tree = GetBaseTree(fTreeName.Data()))) return;
  if (!fObject) {
    Fail("Register", "object not initialized to point to a %s object", 
	 GetTitle());
    return;
  }
  fBranch = tree->Branch(fBranchName.Data(), GetTitle(), &fObject);
  if (!fBranch) {
    Fail("Register", "couldn't make output branch %s in tree %s", 
	 fBranchName.Data(), fTreeName.Data());
    return;
  }
}

//____________________________________________________________________ 
//  
// EOF
//
