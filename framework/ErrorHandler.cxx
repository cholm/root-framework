//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/ErrorHandler.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:21:38 2004
    @brief   Implementation of special error handler 
*/
#include <TSystem.h>
#include <TError.h>
#include <TROOT.h>
#include <iostream> 
#include <iomanip> 
#include <string> 
#include <cstdlib> 
#define WIDTH 78

namespace Framework 
{

  /** Function to handle error/warning/debug/... messages. 
      @ingroup fw_steer 
      
      This is a specialisation of the default error handler which is
      installed when the Framework library is loaded.  it handles the
      levels kInfo+100 as verbatim messages, and kInfo+200 as debug
      messages.   Also, it formats the output so that it fits within
      78 character terminals.   Finally, it prints messages to
      std::clog rather than std::cerr. 

      @param level Type of message 
      @param abort Whether to abort 
      @param location Where the message came from 
      @param msg The message 
  */
  void
  ErrorHandler(int level, Bool_t abort, const char *location, const char *msg) 
  {
    if (level < gErrorIgnoreLevel) return;
    
    std::string emsg;
    
    if (level >= kInfo)     emsg = "Info";
    if (level >= kInfo+100) emsg = "Message";
    if (level >= kInfo+200) emsg = "Debug";
    if (level >= kWarning)  emsg = "Warning";
    if (level >= kError)    emsg = "Error";
    if (level >= kBreak)    emsg = "\n *** Break ***";
    if (level >= kSysError) emsg = "SysError";
    if (level >= kFatal)    emsg = "Fatal";

    if (level >= kBreak && level < kSysError)
      emsg.append(": ");
    else if (!location || strlen(location) == 0)
      emsg.append(" ");
    else {
      emsg.append(" in <");
      emsg.append(location);
      emsg.append(">: ");
    }
    emsg.append(msg);

    size_t cur = 0;
    size_t i   = 0;
    do {
      if (i == 1) TROOT::IncreaseDirLevel();
      size_t dir   = (i == 0 ? 0 : gROOT->GetDirLevel());
      size_t space;
      if (emsg.size() - cur <= WIDTH - dir) {
	space = emsg.size();
      }
      else {
	space = emsg.find_last_of(' ', cur + WIDTH - dir);
	if (space == std::string::npos) 
	  space = std::min(cur + WIDTH - dir,emsg.size());
      }
      if (dir > 0) std::clog << std::setw(dir) << " " << std::flush;
      std::clog << emsg.substr(cur, space - cur) << std::endl;
      cur = space + 1;
      i++;
    } while (cur < emsg.size());
    if (i > 1) TROOT::DecreaseDirLevel();
    
    if (abort) {
      std::clog << "aborting" << std::endl;
      if (gSystem) {
	gSystem->StackTrace();
	gSystem->Abort();
      } else
	::abort();
    }
  }

  /** @class __Init framework/ErrorHandler.cxx <framework/Task.h>
      @brief Static initializer. 
      @ingroup steer 
   */
  struct __Init 
  {
    static __Init* fgInstance;
    __Init() 
    {
      SetErrorHandler(&ErrorHandler);
    }
  };
  __Init* __Init::fgInstance = new __Init;
}

  
