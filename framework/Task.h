// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/Task.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:23:18 2004
    @brief   Task declaration file 
*/
#ifndef Framework_Task
#define Framework_Task
#include <TTask.h>
#include <TError.h>
class TFolder;
class TBranch;
class TTree;
class TCollection;
class TError;
class TBrowser;
class TDirectory;

namespace Framework 
{
  /** @defgroup fw_tasks Task classes. 
      @brief This group contains a number of task classes, including
      the base class Framework::Task, as well as some more specialised
      tasks. 

      Each Task has it's own TFolder, TBranch, and TCollection.  The
      idea is, that a task will either:
      		
      <dl>
        <dt> Writer mode </dt>
	<dd>
	  <ol>
            <li> 
	      Read data from some TFolder 
	    </li>
	    <li> Process that data to make new data, which is stored
	      in the cache, which is put into the TBranch on  an
	      output TTree, and put into the TFolder so that
	      subsequent tasks may use that data.       
	    </li>
	  </ol>
	  @see ArrayWriter
	  @see ObjectWriter
	</dd>
	<dt> Reader mode </dt>
	<dd>
	  <ol>
	    <li> 
	      Read some data from the TBranch on an input tree
            </li>
	    <li>
	      Stored it on the TFolder, so that subsequent tasks may
              access that data
	    </li>
          </ol>
	  @see ArrayReader
	  @see ObjectReader
        </dd>
      </dl>
      									    
      A simple example of a writer could be
      @dontinclude example/LineWriter.h 
      @skip  class LineWriter
      @until };
      with implementation 
      @dontinclude example/LineWriter.cxx 
      @skip LineWriter::LineWriter 
      @until // EOF
      
      Suppose an input file contained a TTree with a TBranch called
      "points", with a TClonesArray of Hit, then the to above tasks
      could be combined so that a job would read the Points from the
      input file, and tracking would be done, and Line object written
      to a TBranch called "lines" on the output tree.

      @dontinclude example/ReadWriteConfig.C
      @skip { 
      @until }
      
      Task inherits from TTask, so all the usual TTask features are
      avaliable, like setting break points, disabling execution and so
      on.      			

      Notice the methods
      @code 
        Stop(const Char_t* where, const format, ...);			    
        Fail(const Char_t* where, const format, ...);			    
        Abort(const Char_t* where, const format, ...);			    
      @endcode 							    
      which are equivilant to TObject::Warning, TObject::Error, and
      TObject::Fatal, but differs in that the fStatus member is set,
      and the tasks is possibly disabled.  Also the fStatus of the
      Main task is also set, so that the job will end.  This was
      needed, as the normal TObject are not virtual.  If
      TObject::DoError was virtual, it could all have been handled
      there - alas, it's not.
      									    
      There also two convinence methods
      @code 
        Verbose(Int_t when, const Char_t* where, const format, ...);	    
        Debug(Int_t when, const Char_t* where, const format, ...);
      @endcode 
      which behaves like TObject::Info, but only emits the stuff to
      stdout if fVerbose and fDebug (set via SetVerbose and SetDebug)
      is larger than or equal to when respectively.  This is useful
      for debugging and messages from the tasks.  

      @see Framework::Main
  */

  /** @class Task framework/Task.h <framework/Task.h>
      @ingroup fw_tasks 
      @brief Element of execution class (a task)

      @see tasks
  */
  class Task : public TTask
  {
  public:
    /** Status codes */
    enum {
      /** All is fine */
      kOk     = 0,
      /** Could not complete current task */
      kStop   = kWarning + 100,
      /** Failed at current task, can not to more */
      kFail   = kError - 100,
      /** Shouldn't happen - fail job altogether */
      kAbort  = kError + 100    
    };

    /** Construct a task. 
	@param name Name of the task
	@param title Title of the task  */
    Task(const Char_t* name="", const Char_t* title="");
    /** Destruct the task.   */
    virtual ~Task() {}
    
    /**@{*/
    /** @name Navigation member functions */
    /** Get the folder that corresponds to this task 
	@return  the tasks folder */
    virtual TFolder* GetFolder() const { return fFolder; }
    /** Get the tasks branch 
	@return branch of this task  */
    virtual TBranch* GetBranch() const { return fBranch; }
    /** Browse this task 
	@param b Browser to use */
    virtual void Browse(TBrowser* b);
    /** Add a taks to the list of tasks.   Added object must be a
	pointer to a object of (sub)class Framework::Task 
	@param task Task to add to list of tasks. */
    virtual void Add(TTask* task);
    /**@}*/
    
    /**@{*/
    /** @name Main excution member functions */
    /** Execute a command.  
	@param option passed on to the methods directly */
    virtual void Exec(Option_t* option="make") = 0;
    /**@}*/

    /**@{*/
    /** @name Initialization member functions */
    /** Execute Initialize on this and all sub tasks. 
	@param option Option string */
    virtual void ExecInitialize(Option_t* option=""); //*MENU*
    /** Register the folders 
	@param option Not used - Depends on derived class */
    virtual void Initialize(Option_t* option=""); 
    /**@}*/

    /**@{*/
    /** @name Register member functions */
    /** Execute Register() on this and all sub-tasks.
        @param option Option string */
    virtual void ExecRegister(Option_t* option=""); //*MENU*
    /** Register brances 
	@param option Depends on derived class */
    virtual void Register(Option_t* option="") = 0;
    /**@}*/

    /**@{*/
    /** @name Exec member functions */
    /** Execute Exec() on this and all sub-tasks.
        @param option Option string */
    virtual void ExecuteTask(Option_t* option=""); //*MENU*
    /**@}*/

    /**@{*/
    /** @name Finish member functions */
    /** Execute Finish() on this and all sub-tasks.
        @param option Option string */
    virtual void ExecFinish(Option_t* option=""); //*MENU*
    /** De-register folder. 
	@param option Depends on derived class */
    virtual void Finish(Option_t* option="");
    /**@}*/

    /**@{*/
    /** @name Reset functions */
    /** Execute Reset() on this and all sub-tasks.
        @param option Option string */
    virtual void ExecReset(Option_t* option=""); //*MENU*
    /** Resets the status of the object to @c kOk 
	@param option Not used. */
    virtual void Reset(Option_t* option="") { fStatus = kOk; }
    /**@}*/
    
    /**@{*/
    /** @name Execution control member functions */
    /** Get the status of the object 
	@return  Objects status */
    virtual Int_t GetStatus() const { return fStatus; }
    /** Set the status of the object 
	@param x The status level to set objects status to */
    virtual void SetStatus(Int_t x=kOk) { fStatus = x; } //*MENU*
    /** Set status to kStop and flag Main to stop (set the fStatus of
	that to kStop too).  Task is still active 
	@param l Message format string. 
	@param f @c printf -like format string */
    virtual void Stop(const Char_t* l, const Char_t* f, ...); 
    /** Set status to kFail and flag Main to fail (set the fStatus of
	that to kFail too).  Task is still deactivated
	@param l Message format string. 
	@param f @c printf -like format string */
    virtual void     Fail(const Char_t* l, const Char_t* f, ...); 
    /** Set status to kAbort and flag Main to abort (set the fStatus of
	that to kAbort too).  Task is still deactivated
	@param l Message format string.
	@param f @c printf -like format string */
    virtual void Abort(const Char_t* l, const Char_t* f, ...);
    virtual void Abort() { Abort("",""); }
    /**@}*/

    /**@{*/
    /** @name Messaging member functions */
    /** Get the debug level
	@return The debug level */
    virtual Int_t GetDebug() const { return fDebug; }
    /** Get the verbosity level
	@return The verbosity level */
    virtual Int_t GetVerbose() const { return fVerbose; }    
    /** Set the debug level 
	@param x Level to set the debugging at */
    virtual void SetDebug(Int_t x=0);     // *MENU*
    /** Set the verbostiy level 
	@param x Level to set the verbostiyging at */
    virtual void SetVerbose(Int_t x=0); // *MENU*
    /** Print a message conditional on the debug level.  If the level
	is set to 50 or more, the task will print a lot of
	information. 
	@param lvl If @c >= @c fDebug the message is printed
	@param l Location
	@param f @c printf -like format string */
    virtual void Debug(Int_t lvl, const Char_t* l, 
		       const Char_t* f, ...) const;
    /** Print a message conditional on the verbostiy level
	If the verbosity is set to 10 or more, some tasks will print
	some information. 
	@param lvl If @c >= @c fVerbose the message is printed
	@param l Location
	@param f @c printf -like format string */
    virtual void Verbose(Int_t lvl, const Char_t* l, 
			 const Char_t* f, ...) const;
    /** Print information on the task
	@param option A combination of letters
	<dl>
	  <dt> @c D </dt>
	  <dd> Print details, like title, verbosity, debug level,
	    folder, branch, and cache name. </dd>
	  <dt> @c R [@e n] </dt>
	  <dd> Print all contained sub-tasks. If the number @e n (@f$
	    < 10@f$) is specified, then only up to that level of
	    recursion is done 
	  </dd> 
	</dl>
    */
    virtual void Print(Option_t* option="DR") const; //*MENU*
    /**@}*/    
  protected:
    /** Parent task */
    Task* fParent;    
    /** Folder used */
    TFolder* fFolder;
    /** The branch used */
    TBranch* fBranch; //
    /** The cache used */
    TCollection* fCache; //!
    /** Debug level of this object */
    Int_t fDebug;
    /** Verbosity level of this object */
    Int_t fVerbose;
    /** Status of this object */
    mutable Int_t fStatus; 

    /** Get the base tree 
	@param name Name of the tree to search for.
	@return requested tree if found, or zero on error  */
    TTree* GetBaseTree(const Char_t* name);
    /** Get the top-level folder 
	@return The top-level folder, or 0 if not found  */
    TFolder* GetBaseFolder();

    /**@{*/
    /** @name Internal embre functions to do stuff before actually
	sending the various messages to the task and sub tasks. */
    /** Method called before running a method of a task. 
	@param mName Name of method
	@param option Option passed to method
	@return  false if task isn't active, a break point is set, or
	that the BreakIn is set on this task.   Otherwise true, and
	the calling function can call the specific function. */
    Bool_t PreExec(const Char_t* mName, Option_t* option);
    /** Called after a member function has performed. 
	@return false, if break point is set after the last called
	member function */ 
    Bool_t PostExec();

#ifndef __CINT__ // CINT doesn't understand pointer to member
    /** Execute sub-tasks, member function
	@param name What's being done
	@param func Function to execute 
	@param option Option string */
    void SubExec(const char* name, 
		 void (Task::*func)(Option_t*), Option_t* option="");
#endif
    /** Send message Exec to all sub-tasks - Hidden
        @param option Option to pass to all sub-tasks */
    virtual void ExecuteTasks(Option_t* option="") {}
    /** Find a TDirectory (e.g., TFile) in the folder hierarchy.
	@param path Path to the directory
	@return Pointer to the directory or null if not found */
    TDirectory* FindDirectory(const char* path);
    /** Write content of tasks folder to TDirectory @a d.  Folders are
	made into `TDirectory` objects.	
	@param d Directory to write to
	@param recurse If true, recurse down folder tree */
    virtual void WriteFolder(TDirectory* d,
			     Bool_t      recurse=true,
			     TFolder*    folder=0);
    /**@}*/
    ClassDef(Task,1) // DOCUMENT ME
  };
};


#endif
//____________________________________________________________________ 
//  
// EOF
//
