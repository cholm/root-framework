// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/TreeIO.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:24:11 2004
    @brief   TreeIO declaration file
*/
#ifndef Framework_TreeIO
#define Framework_TreeIO
#include <framework/Task.h>
class TBrowser;
class TFile;
class TTree;

namespace Framework 
{
  /** @defgroup fw_special_tasks Specialised tasks 
      @ingroup fw_tasks 
      @brief This group contains some specialised task. 

      Teh user can either use these classes directly, over derive
      custom classes to do other stuff.  

      These tasks are provided for convinence. */
      
  //==================================================================
  /** @class TreeIO framework/TreeIO.h  <framework/TreeIO.h>
      @ingroup fw_special_tasks
      @brief Write TTree to TFile

      This task will open the TFile passed in the @a file argument of
      the constructor in the mode specified in the option argument of
      `Initialize'.
      
      and create the TTree called name in that file.  The file is
      opened via TFile::Open, so a file using a protocol for which
      there's registered a handler (see TPluginManager) can be opened. 
  */
  class TreeIO : public Task
  {
  protected:
    /** File to write to */
    TFile*   fFile;
    /** Tree to write to */
    TTree*   fTree;
    /** Last entry written */
    Int_t    fEntry; 
    /** Number of bytes written */
    Int_t    fBytes; 
  public:
    /** Create a writer object 
	@param name Name of tree to write to 
	@param file File to write tree to  */
    TreeIO(const Char_t* name="", const Char_t* file="");
    /** Destruct a writer.  Closes the output tree */
    virtual ~TreeIO();
    
    /** Browse the writer 
	@param b Browser to use */
    virtual void Browse(TBrowser* b);
    /** Open the file, and make a tree in the file.  
	@param option Not used */
    virtual void Initialize(Option_t* option="");
    /** Does nothing */
    virtual void Register(Option_t* option="") {}
    /** Flush data to file, and close file. 
	@param option Not used */
    virtual void Finish(Option_t* option="");
    /** Write information on writer 
        @param option Not used. */
    virtual void Print(Option_t* option="") const; //*MENU*
    
    ClassDef(TreeIO,1) // Write TTree to TFile
  };

  //==================================================================
  /** @class TreeReader framework/TreeIO.h <framework/TreeIO.h>
      @ingroup fw_special_tasks
      @brief Reader of TTree in TFile 
      
      This task will open the TFile passed in the title argument of
      the constructor in READ mode and get the TTree called
      the name of the object in that file.  The file is opened via
      TFile::Open, so a file using a protocol for which there's
      registered a handler (see TPluginManager) can be opened.

      The class registeres the tree in the top-level folder, so that
      tasks may retrieve a pointer to it by doing
      @code 
      TTree* tree = 0;
      if (!(tree = GetBaseTree("T"))) return;
      fBranch = tree->Branch(GetName(), &fCache);
      @endcode 
      where @c "T" is the name of the tree (which is the same as this
      tasks name)
  */
  class TreeReader : public TreeIO
  {
  public:
    /** Construct a tree reader.  
	@param name Name of the tree in @a file 
	@param file File to open  */
    TreeReader(const Char_t* name="", const Char_t* file="");
    /** Destruct the reader.  Closes the file */
    virtual ~TreeReader() {}
    /** Finds the tree in the input file */
    virtual void Initialize(Option_t* option="");
    /** Reads in the next event, or sets status to kStop if there's no
	more events to read */
    virtual void Exec(Option_t* option="");

    ClassDef(TreeReader,1) // Read TTree in TFile
  };


  //==================================================================
  /** @class DelayedTreeReader framework/TreeIO.h  <framework/TreeIO.h>
      @ingroup fw_special_tasks
      @brief Delayed reading of a tree 
      
      Use this task to read in previous entiries from a tree already
      opened by some TreeReader object.   For example 
      @dontinclude DelayReaderConfig.C 
      @skip DelayRead()
      @until "delay"

      The name of the tree must be the second argument to the
      constructor. 
      
      @until point2 

      Next the tasks to read the individual branches are made.   The
      @c delay object makes a clone of the tree @c "T" opened by @c
      reader and posts that in it's folder.  Therefor the tasks @c
      point2 and @c header2 should register their branches in the tree
      @c "delay/T" 

      @until point2 

      Next, we add the tasks to the main task, and the tasks @c point2
      and @c header2 to the @c delay task rather than the main task.   

      @until }
      
      The above configutation will result in 
      <dl>
        <dt> Entry # 0</dt> 
        <dd> The data of entry 0 for the branches @c "point" and @c
	  "header" will be read in by the tasks @c "point" and @c
	  "header".   The tasks @c "header2" and @c "point2" will read
	  nothing in.    
	</dd>
        <dt> Entry # @f$ i = 1 \cdots n - 1@f$</dt> 
        <dd> The data for entry @f$ i@f$ for the branches @c "point"
	  and @c "header" will be read in by the tasks @c "point" and
	  @c "header".   The tasks @c "header2" and @c "point2" will
	  read in the data for entry @f$ i - 1@f$ for the branches 
	  @c "point" and @c "header". 
	</dd>
      </dl>
      One can define tasks that analysis the data of the current entry
      and the previous entry to do for example mixed event analysis or
      background distributions. 
      
      @note Unfortunately this class does not work with a
      ChainReader.  That's because the member function @c
      TTree::CloneTree does not work properly for @c TChain. 
      
  */
  class DelayedTreeReader : public TreeReader
  {
  protected:
    /** Number of entries to skip back/ahead */
    Int_t fSkip;
    /** The tree name */
    TString fTreeName;
  public:
    /** Create a writer object 
	@param name Name of task
	@param tree Name of the tree
	@param file Name of tree to read from  */
    DelayedTreeReader(const Char_t* name="", 
		      const Char_t* tree="",
		      const Char_t* file="");
    /** Destruct a writer.  Closes the output tree */
    virtual ~DelayedTreeReader() {}
    
    /** Set the number of entires to skip back/ahead
	@param skip Skip number. */
    virtual void SetSkip(Int_t skip=1) { fSkip = skip; }
    /** @return  Get the  number of skip entries */
    virtual Int_t Skip() const { return fSkip; }
    /** Open the file, and make a tree in the file.  
	@param option Not used */
    virtual void Initialize(Option_t* option="");
    /** Does nothing */
    virtual void Register(Option_t* option="") {}
    /** Reads in the next event, or sets status to kStop if there's no
	more events to read */
    virtual void Exec(Option_t* option="");
    /** Write information on writer 
        @param option Not used. */
    virtual void Print(Option_t* option="") const; //*MENU*
    
    ClassDef(DelayedTreeReader,1) // Write TTree to TFile
  };

  //==================================================================
  /** @class TreeWriter framework/TreeIO.h <framework/TreeIO.h>
      @ingroup fw_special_tasks
      @brief Write TTree to TFile

      This task will open the TFile passed in the title argument of
      the constructor in RECREATE mode and create the TTree called
      name in that file.  The file is opened via TFile::Open, so a
      file using a protocol for which there's registered a handler
      (see TPluginManager) can be opened.
 
      The class registeres the tree in the top-level folder, so that
      tasks may retrieve a pointer to it by doing
      @code 
      TTree* tree = 0;
      if (!(tree = GetBaseTree("T"))) return;
      fBranch = tree->Branch(GetName(), &fCache);
      @endcode 
      where @c "T" is the name of the tree (which is the same as this
      tasks name)
  */
  class TreeWriter : public TreeIO
  {
  public:
    /** Create a writer object 
	@param name Name of tree to write to 
	@param file File to write tree to */
    TreeWriter(const Char_t* name="", const Char_t* file="");
    /** Destruct a writer.  Closes the output tree */
    virtual ~TreeWriter() {}
    /** Open the file, and make a tree in the file.  
	@param option Not used */
    virtual void Initialize(Option_t* option="");
    /** Fills the tree 
	@param option Not used. */
    virtual void Exec(Option_t* option="");
    /** Flush data to file, and close file. 
	@param option Not used */
    virtual void Finish(Option_t* option="");
    
    ClassDef(TreeWriter,1) // Write TTree to TFile
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
