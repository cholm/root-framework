// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    framework/ObjectIO.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:15 2004
    @brief   ObjectIO declaration file 
*/
#ifndef Framework_ObjectIO
#define Framework_ObjectIO
#include <framework/Task.h>
#include <TString.h>


namespace Framework 
{
  //==================================================================
  /** @class ObjectIO framework/ObjectIO.h <framework/ObjectIO.h>
      @ingroup fw_special_tasks
      @brief Base class for TObject reader and writers 
   */
  class ObjectIO : public Task 
  {
  protected:
    /** Name of the tree to make/get the branch in/from */
    TString fTreeName;
    /** Name of the branch put/get the object in/from */
    TString fBranchName;
    /** Object to read/write */
    TObject* fObject;
  public:
    /** Default constructor  */
    ObjectIO();
    /** Normal constructor 
	@param name Name of task and the folder it uses 
	@param className  Class name of the object
	@param branchName Name of the branch to put the object in
	@param treeName   Name of the tree that the branch should be in */
    ObjectIO(const Char_t* name, 
	     const Char_t* className, 
	     const Char_t* branchName=0, 
	     const Char_t* treeName=0);
    /** Destructor  */
    virtual ~ObjectIO() {}
    /** Set the tree's name 
	@param name Name of the tree */
    virtual void SetTreeName(const Char_t* name) { fTreeName = name; }
    /** Set the branch's name 
	@param name Name of the branch */
    virtual void SetBranchName(const Char_t* name) { fBranchName = name; }
    /** Initialize the task. The object is initialized by the default
	constructor if it hasn't already been initialized  
	@param option Not used.  Passed on to
	Framework::Task::Initialize  */
    virtual void Initialize(Option_t* option="");
    /** Does nothing 
	@param option Not used  */
    virtual void Exec(Option_t* option="") {}
    
    ClassDef(ObjectIO,1) // 
  };

  //==================================================================
  /** @class ObjectReader framework/ObjectIO.h <framework/ObjectIO.h>
      @ingroup fw_special_tasks
      @brief Task to read in a TObject from a TTree 

      Reads one object from a branch in a TTree.   In derived classes,
      the user needs to statically cast the member @c fObject to the
      right class.  For example 
      @dontinclude example/HeaderReader.cxx 
      @skip Exec 
      @until }
   */
  class ObjectReader : public ObjectIO 
  {
  public:
    /** Default constructor  */
    ObjectReader();
    /** Normal constructor 
	@param name Name of task and the folder it uses 
	@param className  Class name of the object
	@param branchName Name of the branch to read the object from
	@param treeName   Name of the tree that the branch is read from */
    ObjectReader(const Char_t* name,
		 const Char_t* className, 
		 const Char_t* branchName=0, 
		 const Char_t* treeName=0);
    /** Destructor  */
    virtual ~ObjectReader() {}
    /** Regster the task.  The branch is obained from the tree, and
	the cache set to the read-in location. 
	@param option Not used.  Passed on to
	Framework::Task::Register  */
    virtual void Register(Option_t* option="");

    ClassDef(ObjectReader,1) // 
  };

  //==================================================================
  /** @class ObjectWriter framework/ObjectIO.h <framework/ObjectIO.h>
      @ingroup fw_special_tasks
      @brief Task to read in a TObject from a TTree 

      Writes one object from a branch in a TTree.   In derived classes,
      the user needs to statically cast the member @c fObject to the
      right class.  For example 
      @dontinclude example/HeaderWriter.cxx 
      @skip Exec 
      @until }
   */
  class ObjectWriter : public ObjectIO 
  {
  public:
    /** Default constructor  */
    ObjectWriter();
    /** Normal constructor 
	@param name Name of task and the folder it uses 
	@param className  Class name of the object
	@param branchName Name of the branch to put the object in
	@param treeName   Name of the tree that the branch should be in */
    ObjectWriter(const Char_t* name, 
		 const Char_t* className, 
		 const Char_t* branchName=0, 
		 const Char_t* treeName=0);
    /** Destructor  */
    virtual ~ObjectWriter() {}
    /** Regster the task.  The branch is obained from the tree, and
	the cache set to the read-in location. 
	@param option Not used.  Passed on to
	Framework::Task::Register  */
    virtual void Register(Option_t* option="");

    ClassDef(ObjectWriter,1) // 
  };
}

#endif
//
// EOF
//
  
      
